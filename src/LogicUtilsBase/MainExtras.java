package LogicUtilsBase;

import LogicUtilsBase.LogicBlocks.Block;
import LogicUtilsBase.LogicBlocks.MSI.SevenSegment;
import LogicUtilsBase.LogicBlocks.NativeBlock;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.stream.IntStream;

import LogicUtilsBase.LogicBlocks.Block.blockType;
import LogicUtilsBase.LogicBlocks.basicBoxes.Box;

/**
 * Created by evan on 4/27/2017. This class contains any static methods used by LogicToolsMain to get stuff working.
 * This primarily exists as a way to shorten LogicToolsMain.
 */
public class MainExtras {
    static String shortenTahoma(int lenScore, String inputText) {
        if(getCharScore(inputText) <= lenScore) return inputText;
        else {
            StringBuilder outString = new StringBuilder();
            int i = 0;
            while (getCharScore(outString.toString()) < lenScore - 4) {
                outString.append(inputText.charAt(i));
                i++;
            }
            if(outString.toString().charAt(outString.length()-1) == ' ') {
                outString.deleteCharAt(outString.length()-1);
            }
            return outString.append("...").toString();
        }
    }

    static int getCharScore(String inputText) {
        int[] charScores = new int[inputText.length()];
        for(int i = 0; i < inputText.length(); i++) {
            char thisChar = inputText.charAt(i);
            if(Character.isLowerCase(thisChar)) {
                if(thisChar == 'm') charScores[i] = 3;
                else if(thisChar == 'f' || thisChar == 'i' || thisChar == 'j' || thisChar == 'l' || thisChar == 'r' || thisChar == 't') {
                    charScores[i] = 1;
                }
                else charScores[i] = 2;
            }
            else if(Character.isUpperCase(thisChar)) {
                if(thisChar == 'M' || thisChar == 'Q' || thisChar == 'W') {
                    charScores[i] = 4;
                }
                else if(thisChar == 'I' || thisChar == 'J') {
                    charScores[i] = 2;
                }
                else charScores[i] = 3;
            }
            else {
                charScores[i] = 2;
            }
        }
        return IntStream.of(charScores).sum();
    }

    @FXML
    public static void popUpWindow(Stage masterStage, String prompt, String windowTitle) {
        Stage newStage = new Stage();
        newStage.initModality(Modality.APPLICATION_MODAL);
        newStage.setTitle(windowTitle);
        newStage.initOwner(masterStage);
        newStage.setResizable(false);

        GridPane grid = new GridPane();

        Text errorText = new Text();
        errorText.setText(prompt);

        Button close = new Button();
        close.setText("Close");
        close.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                newStage.close();
            }
        });

        ColumnConstraints column1 = new ColumnConstraints(200,200,200, Priority.SOMETIMES, HPos.LEFT,false);
        ColumnConstraints column2 = new ColumnConstraints(100,100,100,Priority.SOMETIMES,HPos.RIGHT,false);
        RowConstraints row1 = new RowConstraints(60);
        RowConstraints row2 = new RowConstraints(60);

        grid.add(errorText, 0, 0, 2, 1);
        grid.add(close,1,1,1,1);

        grid.setAlignment(Pos.CENTER);
        grid.getColumnConstraints().add(column1);
        grid.getColumnConstraints().add(column2);
        grid.getRowConstraints().add(row1);
        grid.getRowConstraints().add(row2);

        Scene outScene = new Scene(grid,350,100);
        newStage.setScene(outScene);
        newStage.show();
    }

    @FXML
    public static void popUpWindow(Stage masterStage, String prompt) {
        Stage newStage = new Stage();
        newStage.initModality(Modality.APPLICATION_MODAL);
        newStage.setTitle("Warning");
        newStage.initOwner(masterStage);
        newStage.setResizable(false);

        GridPane grid = new GridPane();

        Text errorText = new Text();
        errorText.setText(prompt);

        Button close = new Button();
        close.setText("Close");
        close.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                newStage.close();
            }
        });

        ColumnConstraints column1 = new ColumnConstraints(200,200,200,Priority.SOMETIMES,HPos.LEFT,false);
        ColumnConstraints column2 = new ColumnConstraints(100,100,100,Priority.SOMETIMES,HPos.RIGHT,false);
        RowConstraints row1 = new RowConstraints(60);
        RowConstraints row2 = new RowConstraints(60);

        grid.add(errorText, 0, 0, 2, 1);
        grid.add(close,1,1,1,1);

        grid.setAlignment(Pos.CENTER);
        grid.getColumnConstraints().add(column1);
        grid.getColumnConstraints().add(column2);
        grid.getRowConstraints().add(row1);
        grid.getRowConstraints().add(row2);

        Scene outScene = new Scene(grid,350,100);
        newStage.setScene(outScene);
        newStage.show();
    }

    public static blockType blockNameToType(String name) throws Exception {
        switch(name) {
            case "Digital HIGH":
            case "Digital LOW":
            case "AND gate":
            case "OR gate":
            case "NAND gate":
            case "NOR gate":
            case "XOR gate":
            case "XNOR gate":
            case "Inverter":
            case "RS-NOR Latch":
            case "RS-NAND Latch":
            case "RS-NOR Latch with AND enable":
            case "RS-NOR Latch with NAND enable":
            case "RS-NAND Latch with AND enable":
            case "RS-NAND Latch with NAND enable":
            case "D Latch with HIGH enable":
            case "JK Latch with HIGH enable":
            case "D Flip-Flop with rising edge trigger":
            case "D Flip-Flop with falling edge trigger":
            case "JK Flip-Flop with rising edge trigger":
            case "JK Flip-Flop with falling edge trigger":
                return blockType.BASIC;
            case "Hexadecimal to 7-segment display decoder":
                return blockType.NATIVE;
            default:
                throw new Exception();
        }
    }

    public static Box blockNameToBasicBox(String name) throws Exception {
        switch(name) {
            case "Digital HIGH":
                return Box.HIGH;
            case "Digital LOW":
                return Box.LOW;
            case "AND gate":
                return Box.AND;
            case "OR gate":
                return Box.OR;
            case "NAND gate":
                return Box.NAND;
            case "NOR gate":
                return Box.NOR;
            case "XOR gate":
                return Box.XOR;
            case "XNOR gate":
                return Box.XNOR;
            case "Inverter":
                return Box.NOT;
            case "RS-NOR Latch":
                return Box.SRNORLATCH;
            case "RS-NAND Latch":
                return Box.SRNANDLATCH;
            case "RS-NOR Latch with AND enable":
                return Box.SRNANDENANDLATCH;
            case "RS-NOR Latch with NAND enable":
                return Box.SRNORENNANDLATCH;
            case "RS-NAND Latch with AND enable":
                return Box.SRNANDENANDLATCH;
            case "RS-NAND Latch with NAND enable":
                return Box.SRNANDENNANDLATCH;
            case "D Latch with HIGH enable":
                return Box.DENHILATCH;
            case "JK Latch with HIGH enable":
                return Box.JKENHILATCH;
            case "D Flip-Flop with rising edge trigger":
                return Box.DRISINGEDGEFF;
            case "D Flip-Flop with falling edge trigger":
                return Box.DFALLINGEDGEFF;
            case "JK Flip-Flop with rising edge trigger":
                return Box.JKRISINGEDGEFF;
            case "JK Flip-Flop with falling edge trigger":
                return Box.JKFALLINGEDGEFF;
            default:
                throw new Exception();
        }
    }

    public static boolean nameIsBasicBox(String name) {
        switch(name) {
            case "Digital HIGH":
            case "Digital LOW":
            case "AND gate":
            case "OR gate":
            case "NAND gate":
            case "NOR gate":
            case "XOR gate":
            case "XNOR gate":
            case "Inverter":
            case "RS-NOR Latch":
            case "RS-NAND Latch":
            case "RS-NOR Latch with AND enable":
            case "RS-NOR Latch with NAND enable":
            case "RS-NAND Latch with AND enable":
            case "RS-NAND Latch with NAND enable":
            case "D Latch with HIGH enable":
            case "JK Latch with HIGH enable":
            case "D Flip-Flop with rising edge trigger":
            case "D Flip-Flop with falling edge trigger":
            case "JK Flip-Flop with rising edge trigger":
            case "JK Flip-Flop with falling edge trigger":
                return true;
            default:
                return false;
        }
    }

    public static boolean nameIsBasicLatch(String name) {
        switch(name) {
            case "RS-NOR Latch":
            case "RS-NAND Latch":
            case "RS-NOR Latch with AND enable":
            case "RS-NOR Latch with NAND enable":
            case "RS-NAND Latch with AND enable":
            case "RS-NAND Latch with NAND enable":
            case "D Latch with HIGH enable":
            case "JK Latch with HIGH enable":
            case "D Flip-Flop with rising edge trigger":
            case "D Flip-Flop with falling edge trigger":
            case "JK Flip-Flop with rising edge trigger":
            case "JK Flip-Flop with falling edge trigger":
                return true;
            default:
                return false;
        }
    }

    public static String getBlockDescription(String name) {
        switch(name) {
            case "AND gate":
            case "OR gate":
            case "NAND gate":
            case "NOR gate":
            case "XOR gate":
            case "XNOR gate":
                return "Standard " + name + " with user-defined input count.";
            case "Digital HIGH":
                return "No-input block with a single output that always produces a logical 1.";
            case "Digital LOW":
                return "No-input block with a single output that always produces a logical 0.";
            case "Inverter":
                return "Single-input, single-output inverter.";
            case "RS-NOR Latch":
                return "Latch with active high Set and Reset inputs. Includes inverted output.";
            case "RS-NAND Latch":
                return "Latch with active low Set and Reset inputs. Includes inverted output.";
            case "RS-NOR Latch with AND enable":
                return "Latch with active high Set, Reset, and Enable inputs. Includes inverted output.";
            case "RS-NAND Latch with NAND enable":
                return "Latch with active low Set, Reset, and Enable inputs. Includes inverted output.";
            case "RS-NOR Latch with NAND enable":
                return "(rarely used) SR-NOR latch with NAND-based enable gating.";
            case "RS-NAND Latch with AND enable":
                return "(rarely used) SR-NAND latch with AND-based enable gating.";
            case "D Latch with HIGH enable":
                return "D Latch with active high enable. Includes inverted output.";
            case "JK Latch with HIGH enable":
                return "JK Latch with active high enable. Includes inverted output.";
            case "D Flip-Flop with rising edge trigger":
                return "D Flip-Flop with rising edge trigger. Includes inverted output.";
            case "D Flip-Flop with falling edge trigger":
                return "D Flip-Flop with falling edge trigger. Includes inverted output.";
            case "JK Flip-Flop with rising edge trigger":
                return "JK Flip-Flop with rising edge trigger. Includes inverted output.";
            case "JK Flip-Flop with falling edge trigger":
                return "JK Flip-Flop with falling edge trigger. Includes inverted output.";
            case "Hex to 7-segment display decoder":
                return "Convert 4-bit binary value to alphanumeric 7-segment display output.";
        }
        return "-";
    }

    public static NativeBlock generateNativeBlock(String selection, String name, int spec) throws Exception {
        //Any blocks not based on the Block class's architecture are created here, then returned to
        //the addBlock prompt in LogicToolsMain.
        switch(selection) {
            case "Hex to 7-segment display decoder":
                return new SevenSegment(name);
        }
        throw new Exception("MainExtras.generateNativeBlock: selection does not have a specified return NativeBlock!");
    }
}
