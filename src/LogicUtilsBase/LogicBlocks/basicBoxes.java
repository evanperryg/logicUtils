package LogicUtilsBase.LogicBlocks;

import LogicUtilsBase.LogicBlocks.BooleanIO.LatchFF;
import LogicUtilsBase.LogicBlocks.BooleanIO.StandardGates;

import java.util.HashMap;


/**
 * Created by evan on 4/6/2017.
 * this class contains the Block-level definitions for all simple logic gates, latches, and flip-flops. It attaches the
 * algorithmic descriptions of the behavior of various logic blocks defined setIn the BooleanIO package to the HashMaps
 * necessary to add the block into a netlist.
 *
 */
public class basicBoxes {
    private final String netListName;
    private final HashMap<String, Boolean> ins;
    private final HashMap<String, Boolean> outs;
    private LatchFF latch;

    public enum Box {
        HIGH,
        LOW,
        AND,
        OR,
        NOT,
        NAND,
        NOR,
        XOR,
        XNOR,
        SRNORLATCH,
        SRNANDLATCH,
        SRNORENANDLATCH,
        SRNORENNANDLATCH,
        SRNANDENANDLATCH,
        SRNANDENNANDLATCH,
        DENHILATCH,
        JKENHILATCH,
        DRISINGEDGEFF,
        DFALLINGEDGEFF,
        JKRISINGEDGEFF,
        JKFALLINGEDGEFF
    }

    private final Box lbox;

    //used by Block to create simple logic gates
    public basicBoxes(Box bName, HashMap<String,Boolean> ins, HashMap<String,Boolean> outs, String netListName) {
        this.netListName = netListName;
        this.lbox = bName;
        this.ins = new HashMap<>(ins);
        this.outs = new HashMap<>(outs);
        //if there's anything specific to do relating to each block, do it here
        switch (bName) {
            case HIGH:
            case LOW:
            case AND:
            case OR:
            case NOT:
            case NAND:
            case NOR:
            case XOR:
            case XNOR:
                //these are the blocks that may be instantiated using this constructor. Others should throw
                //a NullPointerException.
                break;
            default:
                throw new NullPointerException("invalid block name or incorrect instantiation.");
        }
    }
    //used by Block to create latches and flip flops
    public basicBoxes(Box bName, HashMap<String,Boolean> ins, HashMap<String,Boolean> outs, String netListName, boolean preQ) {
        this.netListName = netListName;
        this.lbox = bName;
        this.ins = new HashMap<>(ins);
        this.outs = new HashMap<>(outs);
        ins.put(netListName+"_in_preQ", preQ);
        //Latches and flip-flops require that a LatchFF object be initialized with the corresponding enum, so
        //that is done here.
        switch (bName) {
            case SRNORLATCH:
                latch = new LatchFF(LatchFF.Latch.SRNOR, preQ);
                break;
            case SRNANDLATCH:
                latch = new LatchFF(LatchFF.Latch.SRNAND, preQ);
                break;
            case SRNORENANDLATCH:
                latch = new LatchFF(LatchFF.Latch.SRNORENAND, preQ);
                break;
            case SRNORENNANDLATCH:
                latch = new LatchFF(LatchFF.Latch.SRNORENNAND, preQ);
                break;
            case SRNANDENANDLATCH:
                latch = new LatchFF(LatchFF.Latch.SRNANDENNAND, preQ);
                break;
            case SRNANDENNANDLATCH:
                latch = new LatchFF(LatchFF.Latch.SRNANDENNAND, preQ);
                break;
            case DENHILATCH:
                latch = new LatchFF(LatchFF.Latch.DLATCHENHI, preQ);
                break;
            case JKENHILATCH:
                latch = new LatchFF(LatchFF.Latch.JKLATCHENHI, preQ);
                break;
            case DRISINGEDGEFF:
            case DFALLINGEDGEFF:
                latch = new LatchFF(LatchFF.Latch.DFLIP, preQ);
                break;
            case JKRISINGEDGEFF:
            case JKFALLINGEDGEFF:
                latch = new LatchFF(LatchFF.Latch.DFLIP, preQ);
                break;
            default:
                throw new NullPointerException("invalid block name or incorrect instantiation.");
        }
    }

    public boolean out(String outputName) {
        // method for producing the output of a given gate. In fact, all outputs are calculated, but only the requested
        // output is returned.
        // even if it only has one output, force the identification of the outputname to ensure the reference is valid.
        Boolean[] m = ins.values().toArray(new Boolean[0]);
        //Boolean r;
        switch (lbox) {
            case HIGH:
                outs.put(netListName+"_out_0",true);
                return true;
            case LOW:
                outs.put(netListName+"_out_0",false);
                return false;
            case AND:
                outs.put(netListName+"_out_0", StandardGates.and(m));
                break;
            case OR:
                outs.put(netListName+"_out_0",StandardGates.or(m));
                break;
            case NOT:
                outs.put(netListName+"_out_0",StandardGates.not(m[0]));
                break;
            case NAND:
                outs.put(netListName+"_out_0",StandardGates.nand(m));
                break;
            case NOR:
                outs.put(netListName+"_out_0",StandardGates.nor(m));
                break;
            case XOR:
                outs.put(netListName+"_out_0",StandardGates.xor(m));
                break;
            case XNOR:
                outs.put(netListName+"_out_0",StandardGates.xnor(m));
                break;
            case SRNORLATCH:
            case SRNANDLATCH:
                latch.latch(ins.get(netListName+"_in_S"), ins.get(netListName+"_in_R"));
                outs.put(netListName+"_out_Q", latch.q);
                outs.put(netListName+"_out_Qn", latch.qn);
                break;
            case SRNORENANDLATCH:
            case SRNORENNANDLATCH:
            case SRNANDENANDLATCH:
            case SRNANDENNANDLATCH:
                latch.latch(ins.get(netListName+"_in_S"), ins.get(netListName+"_in_R"), ins.get(netListName+"_in_En"));
                outs.put(netListName+"_out_Q", latch.q);
                outs.put(netListName+"_out_Qn", latch.qn);
                break;
            case DENHILATCH:
                latch.latch(ins.get(netListName+"_in_D"), ins.get(netListName+"_in_En"));
                outs.put(netListName+"_out_Q", latch.q);
                outs.put(netListName+"_out_Qn", latch.qn);
                break;
            case JKENHILATCH:
                latch.latch(ins.get(netListName+"_in_J"), ins.get(netListName+"_in_K"), ins.get(netListName+"_in_En"));
                outs.put(netListName+"_out_Q", latch.q);
                outs.put(netListName+"_out_Qn", latch.qn);
                break;
            case DRISINGEDGEFF:
                latch.latch(ins.get(netListName+"_in_D"), ins.get(netListName+"_in_Clk"));
                outs.put(netListName+"_out_Q", latch.q);
                outs.put(netListName+"_out_Qn", latch.qn);
                break;
            case DFALLINGEDGEFF:
                latch.latch(ins.get(netListName+"_in_D"), !ins.get(netListName+"_in_Clk"));
                outs.put(netListName+"_out_Q", latch.q);
                outs.put(netListName+"_out_Qn", latch.qn);
                break;
            case JKRISINGEDGEFF:
                if (ins.get(netListName+"_in_J") && ins.get(netListName+"_in_K")) {
                    //toggle
                    latch.latch(latch.qn, ins.get(netListName+"_in_Clk"));
                } else if (ins.get(netListName+"_in_J") && !ins.get(netListName+"_in_K")) {
                    //set low
                    latch.latch(false, ins.get(netListName+"_in_Clk"));
                } else if (!ins.get(netListName+"_in_J") && ins.get(netListName+"_in_K")) {
                    //set high
                    latch.latch(true, ins.get(netListName+"_in_Clk"));
                }
                outs.put(netListName+"_out_Q", latch.q);
                outs.put(netListName+"_out_Qn", latch.qn);
                break;
            case JKFALLINGEDGEFF:
                if (ins.get(netListName+"_in_J") && ins.get(netListName+"_in_K")) {
                    //toggle
                    latch.latch(latch.qn, !ins.get(netListName+"_in_Clk"));
                } else if (ins.get(netListName+"_in_J") && !ins.get(netListName+"_in_K")) {
                    //set low
                    latch.latch(false, !ins.get(netListName+"_in_Clk"));
                } else if (!ins.get(netListName+"_in_J") && ins.get(netListName+"_in_K")) {
                    //set high
                    latch.latch(true, !ins.get(netListName+"_in_Clk"));
                }
                outs.put(netListName+"_out_Q", latch.q);
                outs.put(netListName+"_out_Qn", latch.qn);
                break;
        }
        //this allows some wiggle room setIn case of syntax mistakes.
        if(outs.containsKey(outputName)) {
            return outs.get(outputName);
        }
        else if(outs.containsKey(netListName+"_"+outputName)) {
            return outs.get(netListName+"_"+outputName);
        }
        else if(outs.containsKey(netListName+"_in_"+outputName)) {
            return outs.get(netListName+"_in_"+outputName);
        }
        else throw new NullPointerException("basicBoxes.readOut for netListName " + netListName + ": outputName " + outputName + " not found.");
    }

    public void in(String inNet, Boolean j) {
        if(ins.containsKey(inNet)) {
            ins.replace(inNet, j);
        }
        else if(ins.containsKey(netListName+"_"+inNet)) {
            ins.replace(netListName+"_"+inNet,j);
        }
        else if(ins.containsKey(netListName+"_in_"+inNet)) {
            ins.replace(netListName+"_in_"+inNet,j);
        }
        else throw new NullPointerException("basicBoxes.setIn for netListName " + netListName + ": inNet " + inNet + " not found.");
    }

    public boolean inRead(String netName) {
        if(ins.containsKey(netName)) {
            return ins.get(netName);
        }
        else if(ins.containsKey(netListName+"_"+netName)) {
            return ins.get(netListName+"_"+netName);
        }
        else if(ins.containsKey(netListName+"_in_"+netName)) {
            return ins.get(netListName+"_in_"+netName);
        }
        else throw new NullPointerException("basicBoxes.readIn for netListName " + netListName + ": netName " + netName + " not found.");
    }

    public boolean netType(String netName) {
        if (outs.containsKey(netName) || outs.containsKey(netListName+"_"+netName) || outs.containsKey(netListName+"_in_"+netName)) return true;
        else if (ins.containsKey(netName) || ins.containsKey(netListName+"_"+netName) || ins.containsKey(netListName+"_in_"+netName))return false;
        else throw new NullPointerException("basicBoxes.getType for netListName " + netListName + ": netName " + netName + " not found.");

    }

    Box basicBoxType() { return lbox; }

    public static boolean isStanardGate(Box b) {
        switch(b) {
            case HIGH:
            case LOW:
            case AND:
            case OR:
            case NOR:
            case NOT:
            case XOR:
            case NAND:
            case XNOR:
                return true;
            default:
                return false;
        }
    }

}
