package LogicUtilsBase.LogicBlocks.BooleanIO;

import org.jetbrains.annotations.Contract;

/**
 * Created by evan on 3/29/2017.
 * Contains algorithmic descriptions for basic logic gates with 1 output and any number of inputs. There is no
 * StandardGates object, but the methods included in the StandardGates class act as descriptors for the behavior of
 * virtually every block defined under the basicBoxes class.
 */
public class StandardGates {
    @Contract(pure = true)
    public static boolean and(Boolean... m) {
        boolean r = m[0];
        for (int i = 1; i < m.length; i++) r = r && m[i];
        return r;
    }
    @Contract(pure = true)
    public static boolean or(Boolean... m) {
        boolean r = m[0];
        for (int i = 1; i < m.length; i++) r = r || m[i];
        return r;
    }
    @Contract(pure = true)
    public static boolean not(Boolean i) {
        return !i;
    }
    @Contract(pure = true)
    public static boolean nand(Boolean... m) {
        boolean r = m[0];
        for (int i = 1; i < m.length; i++) r = r && m[i];
        return !r;
    }
    @Contract(pure = true)
    public static boolean nor(Boolean... m) {
        boolean r = m[0];
        for (int i = 1; i < m.length; i++) r = m[i] || r;
        return !r;
    }
    @Contract(pure = true)
    public static boolean xor(Boolean... m) {
        int countOnes = 0;
        for (Boolean aM : m) if (aM) countOnes++;
        return countOnes % 2 != 0;
    }
    @Contract(pure = true)
    public static boolean xnor(Boolean... m) {
        int cnt = 0;
        for (Boolean aM : m) if (aM) cnt++;
        return cnt % 2 == 0;
    }
}
