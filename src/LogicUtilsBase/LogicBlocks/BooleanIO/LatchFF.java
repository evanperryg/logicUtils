package LogicUtilsBase.LogicBlocks.BooleanIO;

import static LogicUtilsBase.LogicBlocks.BooleanIO.StandardGates.*;

/**
 * Created by evan on 4/4/2017.
 * Class containing the logical behavior of flip flops and latches. Serves the exact same purpose as StandardGates-
 * this class only contains the logical calculations that determine the next state of a given Latch or Flip-Flop.
 * a LatchFF object cannot be directly added to a netlist, nor can the Block class directly inherit a LatchFF object,
 * as LatchFF does not have any way to handle the nets for a block. basicBoxes instantiates a LatchFF object and
 * assigns nets to the logical inputs and outputs of the LatchFF object. In this manner, and only setIn this manner, can
 * a block based on the LatchFF class be created.
 *
 */
public class LatchFF {
    public boolean q, qn;
    private boolean pr, ps, dp, ckp, jp, kp, e;
    private srNand srNand;
    private srNand srNand2;
    private srNand3 srNand3;
    private srNorEnAnd srNorEnAnd;
    public enum Latch {
        SRNOR,SRNAND,SRNORENAND,SRNORENNAND,SRNANDENAND,SRNANDENNAND,DLATCHENHI,JKLATCHENHI,DFLIP
    }

    private final Latch lType;

    public LatchFF(Latch lType, boolean preQ) {
        this.lType = lType;
        q = preQ;
        qn = !preQ;
        switch(lType) {
            case SRNOR:
                break;
            case SRNAND:
                break;
            case SRNORENAND:
                break;
            case SRNORENNAND:
                break;
            case SRNANDENAND:
                break;
            case SRNANDENNAND:
                break;
            case DLATCHENHI:
                srNorEnAnd = new srNorEnAnd(q);
                break;
            case JKLATCHENHI:
                break;
            case DFLIP:
                srNand = new srNand(q);
                srNand2 = new srNand(false);
                srNand3 = new srNand3(false);
                break;
        }
    }

    public void latch(boolean sd, boolean rcken) {
        switch(lType) {
            case SRNOR:
            case SRNAND:
            case DLATCHENHI:
            case DFLIP:
                pr = rcken;
                ps = sd;
                dp = sd;
                ckp = rcken;
                e = rcken;
                latch();
                break;
            default:
                throw new NullPointerException("invalid reference to latch(s,r)");
        }
    }

    public void latch(boolean sj, boolean rk, boolean enck) {
        switch(lType) {
            case SRNORENAND:
            case SRNORENNAND:
            case SRNANDENAND:
            case SRNANDENNAND:
            case JKLATCHENHI:
                pr = rk;
                ps = sj;
                jp = sj;
                kp = rk;
                e = enck;
                latch();
                break;
            default:
                throw new NullPointerException("invalid reference to latch(s/j,r/k,en)");
        }
    }

    private void latch() {
        switch(lType) {
            case SRNOR:
                q = nor(pr, qn);
                qn = nor(ps, q);
                break;
            case SRNAND:
                q = nand(pr, qn);
                qn = nand(ps, q);
                break;
            case SRNORENAND:
                q = nor(and(pr,e), qn);
                qn = nor(and(ps,e), q);
                break;
            case SRNORENNAND:
                q = nor(nand(pr,e), qn);
                qn = nor(nand(ps,e), q);
                break;
            case SRNANDENAND:
                q = nand(and(pr,e), qn);
                qn = nand(and(ps,e), q);
                break;
            case SRNANDENNAND:
                q = nand(nand(pr,e), qn);
                qn = nand(nand(ps,e), q);
                break;
            case DLATCHENHI:
                srNorEnAnd.latch(not(dp),dp,e);
                q = srNorEnAnd.q;
                qn = srNorEnAnd.qn;
                break;
            case JKLATCHENHI:
                q = nand(qn,nand(jp, e, qn));
                qn = nand(q,nand(kp, q, q));
                break;
            case DFLIP:
                srNand3.latch(srNand2.qn,ckp,dp);
                srNand2.latch(srNand3.qn,ckp);
                srNand.latch(srNand2.qn,srNand3.q);
                q = srNand.q;
                qn = srNand.qn;
                break;
        }
    }


//    public static class srNandEnAnd {
//        public boolean q, qn;
//        private boolean pr, ps, e;
//        public srNandEnAnd(boolean preQ) {
//            q = preQ;
//            qn = !preQ;
//        }
//        public void latch(boolean s, boolean r, boolean en) {
//            pr = r;
//            ps = s;
//            e = en;
//            latch();
//        }
//        void latch() {
//
//        }
//    }
//    public static class srNandEnNand {
//        public boolean q, qn;
//        private boolean pr, ps, e;
//        public srNandEnNand(boolean preQ) {
//            q = preQ;
//            qn = !preQ;
//        }
//        public void latch(boolean s, boolean r, boolean en) {
//            pr = r;
//            ps = s;
//            e = en;
//            latch();
//        }
//        void latch() {
//
//        }
//    }

    private static class srNand3 {
        boolean q, qn;
        private boolean pa, pb, pc;
        srNand3(boolean preQ) {
            q = preQ;
            qn = !preQ;
        }
        public void latch(boolean a, boolean b, boolean c) {
            pa = a;
            pb = b;
            pc = c;
            latch();
        }
        void latch() {
            q = nand(qn,pa,pb);
            qn = nand(q,pc);
        }
    }
    private static class srNand {
        boolean q, qn;
        private boolean pr, ps;
        srNand(boolean preQ) {
            q = preQ;
            qn = !preQ;
        }
        public void latch(boolean s, boolean r) {
            pr = r;
            ps = s;
            latch();
        }
        void latch() {
            q = nand(pr, qn);
            qn = nand(ps, q);
        }
    }
    private static class srNor {
        boolean q, qn;
        private boolean pr, ps;
        public srNor(boolean preQ) {
            q = preQ;
            qn = !preQ;
        }
        public void latch(boolean s, boolean r) {
            pr = r;
            ps = s;
            latch();
        }
        void latch() {
            q = nor(pr, qn);
            qn = nor(ps, q);
        }
    }
    private static class srNorEnAnd {
        boolean q, qn;
        private boolean pr, ps, e;
        public srNorEnAnd(boolean preQ) {
            q = preQ;
            qn = !preQ;
        }
        public void latch(boolean s, boolean r, boolean en) {
            pr = r;
            ps = s;
            e = en;
            latch();
        }
        void latch() {
            q = nor(and(pr,e), qn);
            qn = nor(and(ps,e), q);
        }
    }
}
