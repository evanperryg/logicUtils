package LogicUtilsBase.LogicBlocks;

import LogicUtilsBase.Algorithms.logicInterpreter;
import LogicUtilsBase.etc.AnsiCodes;
import org.jetbrains.annotations.Contract;

import java.util.HashMap;

/**
 * Created by evan on 4/5/2017.
 * The methodology used setIn Vevian allows any blackBox to have up to 52 inputs and 52 ouputs. Because of the way the
 * logicInterpreter backend works, an input and output with the same identifying char do not cross over each other.
 */
public class blackBox {
    private char[] inputs;
    private HashMap<String,Character> inNames = new HashMap<>();
    private HashMap<String,Character> outNames = new HashMap<>();
    private HashMap<Character,Boolean> inVals = new HashMap<>();
    private HashMap<Character,Boolean> outVals = new HashMap<>();
    private HashMap<Character,String> statements = new HashMap<>();

    private final logicInterpreter k;

    //given arrays of input names, output names, and statements, automatically assigns term characters
    public blackBox(String[] inputNames, String[] outputNames, String[] setStatements){
        int i = 0;
        char[] termChars = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
        for(String n : inputNames) {
            inNames.put(n, termChars[i]);
            inVals.put(termChars[i],false);
            i++;
        }
        inputs = new char[i];
        System.arraycopy(termChars,0,inputs,0,i);
        i = 0;
        for(String n : outputNames) {
            outNames.put(n, termChars[i]);
            outVals.put(termChars[i],false);
            i++;
        }
        char[] outputs = new char[i];
        System.arraycopy(termChars,0, outputs,0,i);
        k = new logicInterpreter(false,inputs, outputs, setStatements);
    }
    
    //given hashmaps of input names to input chars, output names to output chars, and output chars to corresponding statements.
    public blackBox(HashMap<String,Character> inputHash, HashMap<String,Character> outputHash, HashMap<Character,String> setStatements) {
        char[] inputs = new char[inputHash.size()];
        char[] outputs = new char[outputHash.size()];
        this.inNames = (HashMap<String,Character>)inputHash.clone();
        this.outNames = (HashMap<String,Character>)outputHash.clone();
        int i = 0;
        for(String n : inputHash.keySet()) {
            inVals.put(inputHash.get(n),false);
            inputs[i] = inputHash.get(n);
            i++;
        }
        this.inputs = inputs;
        i = 0;
        for(String n : outputHash.keySet()) {
            outVals.put(outputHash.get(n),false);
            outputs[i] = outputHash.get(n);
            i++;
        }
        String[] stateArray = new String[outputHash.size()];
        for(int k = 0; k < stateArray.length; k++) {
            //ensures that statements are setIn the same order as corresponding outputs
            stateArray[k] = setStatements.get(outputs[k]);
        }
        this.statements = setStatements;
//        for(String n : outputHash.keySet()) {
//            System.out.println("outputHashKey=" + n + " outputHash=" + outputHash.get(n) + " statement=" + setStatements.get(outputHash.get(n)));
//        }
//        for(int k = 0; k < outputs.length; k++) {
//            System.out.println("outputs[" + k + "]=" + outputs[k] + " statement=" + stateArray[k]);
//        }
        k = new logicInterpreter(false,inputs, outputs, stateArray);
    }

    public void in(String inputName, Boolean j) {
        if(inNames.get(inputName) == null) {
            String[] splitString = inputName.split("_");
            inputName = "";
            for(int i = 1; i < splitString.length; i++) {
                inputName = inputName.concat(splitString[i] + "_");
            }
            inputName = inputName.substring(0,inputName.length()-1);
        }
        inVals.replace(inNames.get(inputName),j);
    }
    public boolean inRead(String inputName) {
        if(inNames.get(inputName) == null) {
            String[] splitString = inputName.split("_");
            inputName = "";
            for(int i = 1; i < splitString.length; i++) {
                inputName = inputName.concat(splitString[i] + "_");
            }
            inputName = inputName.substring(0,inputName.length()-1);
        }
        char i = inNames.get(inputName);
        return inVals.get(i);
    }
    public boolean out(String outputName) {
        if(outNames.get(outputName) == null) {
            String[] splitString = outputName.split("_");
            outputName = "";
            for(int i = 1; i < splitString.length; i++) {
                outputName = outputName.concat(splitString[i] + "_");
            }
            outputName = outputName.substring(0,outputName.length()-1);

        }
        char i = outNames.get(outputName);
        outVals.replace(i,k.solve(i,convert()));
        return outVals.get(i);
    }

    private String convert() {
        String str = "";
        for (char input : inputs) {
            if (inVals.containsKey(input)) {
                if(inVals.get(input)){
                    str = str.concat("1");
                }
                else {
                    str = str.concat("0");
                }
            } else str = str.concat("0");
        }
        return str;
    }
    public boolean netType(String pinName) {
        return outNames.containsKey(pinName);
    }

    @Contract(pure = true)
    public final HashMap<String,Character> getInputNames(){ return inNames; }
    public final HashMap<String,Character> getOutputNames(){ return outNames; }
    public final HashMap<Character,String> getStatements() { return statements; }
}
