package LogicUtilsBase.LogicBlocks;

import java.util.HashMap;
import java.util.List;

/**
 * Created by evan on 5/2/2017. All blocks that aren't described under basicBoxes or blackBox implement the NativeBlock
 * interface. This will keep things simpler when we try to add more blocks down the road, like multiplexers, decoders,
 * ROMS, PALs, and the like.
 */
public interface NativeBlock {
    boolean readOut(String outName);
    boolean readIn(String inName);
    void setNetType(String netName, NetList.netType type);
    void setIn(String inName, boolean value);
    NetList.netType getType(String netName);
    HashMap<String,Boolean> getNets(NetList.netType netType);
    List<String> getNetNames();
    String blockName();
    Object getBlockType();
}
