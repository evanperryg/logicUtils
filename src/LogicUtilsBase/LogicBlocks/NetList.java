package LogicUtilsBase.LogicBlocks;

import LogicUtilsBase.etc.AnsiCodes;
import LogicUtilsBase.etc.StringTable;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

import static LogicUtilsBase.LogicBlocks.NetList.netType.*;

/**
 * Created by evan on 4/13/2017.
 * class that controls the relations between Block objects. Think of the Netlist object as containing the properties
 * of a circuit: the blocks setIn the circuit, how they're connected to each other, and the values of the inputs and
 * outputs at any given time.
 */
public class NetList {
    private final String netListName;
    private final HashMap<String,HashSet<String>> wires = new HashMap<>();
    private final HashMap<String,NativeBlock> blockRegistry = new HashMap<>();
    private final HashMap<String,Boolean> nets = new HashMap<>();
    private final HashMap<String,netType> netTypes = new HashMap<>();

    //constructors. Can inherit another Netlist's properties.
    public NetList(String netListName) {
        this.netListName = netListName;
    }
    public NetList(NetList netlist, String netListName) {
        this.nets.putAll(netlist.allNets());
        this.netTypes.putAll(netlist.allNetProperties());
        this.blockRegistry.putAll(netlist.allBlocks());
        this.wires.putAll(netlist.allWireProperties());
        this.netListName = netListName;
    }
    public NetList(NetList netlist) {
        this.nets.putAll(netlist.allNets());
        this.netTypes.putAll(netlist.allNetProperties());
        this.blockRegistry.putAll(netlist.allBlocks());
        this.wires.putAll(netlist.allWireProperties());
        this.netListName = (netlist.name());
    }

    //addNet(): manually add nets
    //nets can be added individually based on values, or they can be added as a HashMap, or it can inherit
    //the nets of another netlist without inheriting its wires.
    public void addNet(HashMap<String,Boolean> someNets) {
        //add some nets
        nets.putAll(someNets);
    } //add nets as defined setIn a hashmap
    public void addNet(String netName,boolean netValue) {
        nets.put(netName,netValue);
    } //add one net
    public void addNet(String netName) {
        nets.put(netName,false);
    } //add one net, assume it's false for now
    public void addNet(NetList netlist) {
        this.nets.putAll(netlist.allNets());
        this.netTypes.putAll(netlist.allNetProperties());
        this.blockRegistry.putAll(netlist.allBlocks());

    } //add all nets included setIn another netlist

    //addNetType(): manually add net properties
    public boolean addNetType(HashMap<String,netType> someNets) {
        HashMap<String,netType> old = netTypes;
        for(String n : someNets.keySet()) {
            if(nets.containsKey(n)) {
                netTypes.put(n,someNets.get(n));
            }
        }
        return !old.equals(netTypes);
    }
    public void addNetType(String netName,netType type) {
        if(nets.containsKey(netName)) netTypes.put(netName,type);
    }
    public void addNetType(NetList netlist) {
        for(String n : netlist.allNetProperties().keySet()) {
            if(nets.containsKey(n)) netTypes.put(n,netlist.allNetProperties().get(n));
        }
    }

    //forceNetType(): sets the modes for nets, regardless of whether they've been given a mode or not.
    public void forceNetType(HashSet<String> netNames,netType p){
        for(String n : netNames) {
            if(nets.containsKey(n)) {
                netTypes.put(n,p);
            }
        }
    }
    public void forceNetType(String netNames,netType p){
        if(nets.containsKey(netNames)) {
            netTypes.put(netNames,p);
        }
    }

    //addBlock(): add the nets for a certain block
    //addBlock will take the given block's pins, add them to the netlist, and assign their properties automatically.
    public void addBlock(NativeBlock block) {
        blockRegistry.put(block.blockName(),block);
        nets.putAll(block.getNets(INPUT));
        nets.putAll(block.getNets(OUTPUT));
        nets.putAll(block.getNets(INOUT));
        //nets.putAll(block.getNets(Block.getType.INOUT));
        for(String i : block.getNets(INPUT).keySet()) {
            netTypes.put(i,INPUT);
        }
        for(String i : block.getNets(OUTPUT).keySet()) {
            netTypes.put(i,OUTPUT);
        }
        for(String i : block.getNets(INOUT).keySet()) {
            netTypes.put(i,INOUT);
        }
    }

    //removeBlock(): remove the given block and its associated nets, and all that net's wire associations.
    public void removeBlock(String blockName) {
        HashSet<String> allAssociatedNets = new HashSet<>(blockRegistry.get(blockName).getNets(INPUT).keySet());
        allAssociatedNets.addAll(blockRegistry.get(blockName).getNets(OUTPUT).keySet());

        //get rid of all wire associations
        for(String wireNames : wires.keySet()) {
            for(String netsToDelete : allAssociatedNets) {
                wires.get(wireNames).remove(netsToDelete);
            }
        }

        //get rid of all getType and nets associations
        for(String netsToDelete : allAssociatedNets) {
            netTypes.remove(netsToDelete);
            nets.remove(netsToDelete);
        }

        //lastly, delete the block from the block registry
        blockRegistry.remove(blockName);
    }

    //addWire(): instantiates a "connection" between two or more nets.
    public void addWire(String wireName, HashSet<String> connects) {
        //validate the connects set
        for(String c : connects) {
            if(!nets.containsKey(c)) throw new NullPointerException("NetList.addWire for wireName " + wireName + ": net " + c + " does not exist setIn this netlist!");
        }
        wires.put(wireName,connects);
    }
    public void addWire(String wireName, List<String> connects) {
        HashSet<String> cons = new HashSet<>();
        for(String c : connects) {
            if(!nets.containsKey(c)) throw new NullPointerException("NetList.addWire for wireName " + wireName + ": net " + c + " does not exist setIn this netlist!");
            else cons.add(c);
        }
        wires.put(wireName,cons);
    }
    public void addWire(String wireName, String... conns) {
        HashSet<String> connects = new HashSet<>(Arrays.asList(conns));
        //validate the connects set
        for(String c : connects) {
            if(!nets.containsKey(c)) throw new NullPointerException("NetList.addWire for wireName " + wireName + ": net " + c + " does not exist setIn this netlist!");
        }
        wires.put(wireName,connects);
    }

    //removeWire(): removes a "connection" created by addWire
    public void removeWire(String wireName) {
        wires.remove(wireName);
    }

    //removeWireConnection(): removes one net from a group of "connections" created previously by addWire
    public void removeWireConnection(String wireName, String netToRemove) {
        wires.get(wireName).remove(netToRemove);
    }

    //addWireConnection(): adds one net to a group of "connections" created previously by addWire
    public void addWireConnection(String wireName, String netToAdd) {
        if(netTypes.containsKey(netToAdd) && wires.containsKey(wireName)) wires.get(wireName).add(netToAdd);
        else {
            if(!netTypes.containsKey(netToAdd)) throw new NullPointerException("NetList.addWireConnection for wireName " + wireName + ": net " + netToAdd + " has not been given a type!");
            if(!wires.containsKey(wireName)) throw new NullPointerException("NetList.addWireConnection for net " + netToAdd + ": wire " + wireName + " does not exist setIn this netlist!");
        }
    }

    //updateAll(): update everything together.
    public void updateAll() throws Exception {
        //update all wires
        for(String w : wires.keySet()) {
            updateWire(w);
        }
        //if any wires have high outputs that have not propagated to their inputs, update everything again.
        //basically, keep updating until there are no nulls.
        int i = 0;
        for(String w : wires.keySet()) {
            if(valueOfWire(w) == null) {
                updateAll(i);
            }
        }
    }
    private void updateAll(int i) throws Exception {
        if(i > 20) throw new Exception("NetList.updateAll() has reached an unacceptable number of recursions.");
        for(String w : wires.keySet()) {
            updateWire(w);
        }
        for(String w : wires.keySet()) {
            if(valueOfWire(w) == null) {
                i++;
                updateAll(i);
            }
        }
    }

    //updateWire(): update all nets associated with blocks on this wire. it's best to update all wires.
    private void updateWire(String wireName) {
        String outNet = "";
        //find an output or simulator setIn net. Prioritize simulator setIn nets.
        boolean simInFound = false;
        for(String relevantNets : wires.get(wireName)) {
            netType n = netTypes.get(relevantNets);
            switch(n) {
                case OUTPUT:
                    if(!simInFound) outNet = relevantNets;
                    break;
                case SIMULATOR_IN:
                    if(!simInFound) outNet = relevantNets;
                    simInFound = true;
                    break;
                default:
                    break;
            }
        }
        //System.readOut.println("for wire " + wireName + "   chosen net: " + outNet);
        if(!outNet.equals("")) updateLocal(outNet); //just to make sure outNet's value is what it should be
        //outnet now contains the net on the wire that's an output. If there's a simulator net,
        //then the simulator net takes priority over the output nets.
        //so, make all the attached nets equal to outnet
        //if none of the nets on the wire are an output, we assume they are false
        //also, update any simulator readout nets that may be attached.
        if(outNet.equals("")) {                     //if the only things on the wire are inputs, assume they're false
            for(String relevantNets : wires.get(wireName)) {
                if((netTypes.get(relevantNets) != OUTPUT) && (netTypes.get(relevantNets) != SIMULATOR_IN)) setNet(relevantNets, false);
            }
        }
        else if(netTypes.get(outNet) == SIMULATOR_IN || netTypes.get(outNet) == OUTPUT) {
            //if outnet is holding a simulator input or a block output, put outNet's value into all input nets and simulator outs on the wire
            for(String relevantNets : wires.get(wireName)) {
                if((netTypes.get(relevantNets) != OUTPUT) && (netTypes.get(relevantNets) != SIMULATOR_IN)) setNet(relevantNets, nets.get(outNet));
            }
        }
        else throw new NullPointerException("Netlist " + netListName +": updateWire for wire " + wireName + ": outNet is not empty, but also does not hold a SIMULATOR_IN or OUTPUT net. outNet=" + outNet);
        //now that the input nets relevant to this wire are updated, update all nets attached
        //to blocks that are attached this wire. we do this after updating nets related directly
        //to the wire to ensure that outputs attached to the wire are not changing while we change
        //the wire's input nets. Think about it like this: if a basic gate has a wire attaching
        //its output to one of its inputs, we don't want to change the output value of the gate
        //until after the current output of the gate has propagated into the input.
        for(String relevantNets : wires.get(wireName)) {
            updateLocal(relevantNets);
        }

    }

    //resetNets(): resets all nets to false.
    public int resetNets() {
        int replacedvals = 0;
        for(String n : nets.keySet()) {
            if(nets.replace(n,false)) replacedvals++;
        }
        return replacedvals;
    }

    //removeNet(): removes a net from the nets and netTypes hashmaps. This should never be used on a net that is part of a block.
    void removeNet(String netName) {
        nets.remove(netName);
        netTypes.remove(netName);
    }

    //setSimNet(): allows the value of individual nets to be changed from outside the class, but only for CombinationalSimulation nets.
    void setSimNet(String netName, boolean netValue) {
        if((netTypes.get(netName) == SIMULATOR_IN || netTypes.get(netName) == SIMULATOR_OUT) && nets.containsKey(netName)) {
            nets.put(netName,netValue);
        }
        else throw new NullPointerException("NetList (" + netListName + "): external class attempted to set net (" + netName + ") of getType (" + netTypes.get(netName) + "), which is not allowed.");
    }

    //some methods to get the properties of the NetList
    public final boolean valueOf(String netName) {
        return nets.get(netName);
    } //returns the value of a single net
    @Nullable
    private Boolean valueOfWire(String wireName) {
        int[] testWire = new int[2];
        for(String net : wires.get(wireName)) {
            if(nets.get(net)) testWire[1]++;
            else if(!nets.get(net)) testWire[0]++;
        }
        if(testWire[0] == 0) return true;
        else if(testWire[1] == 0) return false;
        else return null;
    } //returns the value of a wire. If all nets on the wire aren't the same, returns null
    @Nullable
    final Boolean valueOfSimNet(String simNetName) {
        if(netTypes.get(simNetName) == SIMULATOR_IN || netTypes.get(simNetName) == SIMULATOR_OUT) {
            String correspondingWire;
            for(String w : wires.keySet()) {
                if(wires.get(w).contains(simNetName)) return valueOfWire(w);
            }
        }
        return null;
    } //returns the value at a simulation net. Since simNets don't directly correspond to a block, their value can't be found the same way as other nets.
    @Contract(pure = true)
    public final String name() {
        return netListName;
    } //returns the name of the NetList object
    public final HashMap<String,Boolean> blockNets(String blockName) {
        HashMap<String,Boolean> outMap = new HashMap<>();
        for(String s : nets.keySet()) {
            if(s.startsWith(blockName)) {
                outMap.put(s,nets.get(s));
            }
        }
        return outMap;
    } //returns a hashmap with the names and values of the nets for a given block
    public final String blockSubProperty(String blockName) {
        if(blockRegistry.get(blockName) instanceof Block) {
            return String.valueOf(((Block)blockRegistry.get(blockName)).getBasicBoxesType());
        }
        return "none";
    } //return the block has a subproperty (for example, a basic block's subproperty may be that it's an and gate)
    private HashSet<String> unModedNets() {
        HashSet<String> temp = new HashSet<>(nets.keySet());
        for(String r : netTypes.keySet()) {
            temp.remove(r);
        }
        return temp;
    } //returns a set with the names of nets that exist setIn the nets hashmap, but not the netTypes hashmap
    private HashSet<String> floatingNets() {
        HashSet<String> outSet = new HashSet<>();
        for(String net : nets.keySet()) {
            boolean unWired = true;
            for(String w : wires.keySet()) {
                if(wires.get(w).contains(net)) unWired = false;
            }
            if(unWired) outSet.add(net);
        }
        return outSet;
    } //returns a set with the names of nets that exist setIn the nets hashmap, but aren't setIn any of the sets associated with the wires
    @NotNull
    private HashSet<String> wireConnections(String wireName) {
        return new HashSet<>(wires.get(wireName));
    }
    @NotNull
    final HashSet<String> allWireNames() {
        return new HashSet<>(wires.keySet());
    } //returns a set with all wire names
    public HashSet<String> netWires(String netName) {
        HashSet<String> out = new HashSet<>();
        for(String wnames : wires.keySet()) {
            if(wires.get(wnames).contains(netName)) out.add(wnames);
        }
        return out;
    }


    //some human-friendly readouts of the Netlist's properties.
    public String properties() {
        StringBuilder output = new StringBuilder();
        output.append("Properties of NetList ").append(netListName).append("\n");
        //first, get block names and their nets
        output.append("block properties:\n");
        for(String b : blockRegistry.keySet()) {
            StringTable stringTable = new StringTable(' ',longestBlockName()+3,1);
            output.append("  ").append(stringTable.makeLine(b + ":", String.valueOf(blockNets(b)))).append("\n");
        }
        //then, get wire names and their nets
        output.append("wire properties:\n");
        for(String w : wires.keySet()) {
            StringTable stringTable = new StringTable(' ',longestWireName()+3,6,1);
            output.append("  ").append(stringTable.makeLine(w + ":", String.valueOf(valueOfWire(w)), String.valueOf(wireConnections(w)))).append("\n");
        }
        //now, get every net and show it individually
        output.append("net properties:\n");
        for(String n : nets.keySet()) {
            StringTable stringTable = new StringTable(' ',longestNetName()+3,15,1);
            output.append("  ").append(stringTable.makeLine(n + ":", String.valueOf(netTypes.get(n)), String.valueOf(nets.get(n)))).append("\n");
        }
        //now, check for any warnings. These include unmoded nets(nets that are setIn the nets hashmap but not the netTypes hashmap) and floating nets (nets that aren't attached to a wire)
        output.append("warnings:\n");
        if(!unModedNets().isEmpty()) output.append("  unmoded nets found: ").append(unModedNets()).append("\n");
        if(!floatingNets().isEmpty()) output.append("  floating nets found: ").append(floatingNets()).append("\n");
        if(floatingNets().isEmpty() && unModedNets().isEmpty()) output.append("  no warnings found.");
        return output.toString();
    } //a console-friendly listing of all the NetList's properties. Useful for debugging.
    public String properties(String AnsiColor) {
        StringBuilder output = new StringBuilder();
        output.append(AnsiColor).append("Properties of NetList ").append(netListName).append("\n");

        //first, get block names and their nets
        output.append("block properties:\n").append(AnsiCodes.ANSI_RESET);
        for(String b : blockRegistry.keySet()) {
            StringTable stringTable = new StringTable(' ',longestBlockName()+3,1);
            output.append("  ").append(stringTable.makeLine(b + ":", String.valueOf(blockNets(b)))).append("\n");
        }
        //then, get wire names and their nets
        output.append(AnsiColor).append("wire properties:\n").append(AnsiCodes.ANSI_RESET);
        for(String w : wires.keySet()) {
            StringTable stringTable = new StringTable(' ',longestWireName()+3,6,1);
            output.append("  ").append(stringTable.makeLine(w + ":", String.valueOf(valueOfWire(w)), String.valueOf(wireConnections(w)))).append("\n");
        }
        //now, get every net and show it individually
        output.append(AnsiColor).append("net properties:\n").append(AnsiCodes.ANSI_RESET);
        for(String n : nets.keySet()) {
            StringTable stringTable = new StringTable(' ',longestNetName()+3,15,1);
            output.append("  ").append(stringTable.makeLine(n + ":", String.valueOf(netTypes.get(n)), String.valueOf(nets.get(n)))).append("\n");
        }
        //now, check for any warnings. These include unmoded nets(nets that are setIn the nets hashmap but not the netTypes hashmap) and floating nets (nets that aren't attached to a wire)
        output.append(AnsiColor).append("warnings:\n").append(AnsiCodes.ANSI_RESET);
        if(!unModedNets().isEmpty()) output.append("  unmoded nets found: ").append(unModedNets()).append("\n");
        if(!floatingNets().isEmpty()) output.append("  floating nets found: ").append(floatingNets()).append("\n");
        if(floatingNets().isEmpty() && unModedNets().isEmpty()) output.append("  no warnings found.");

        return output.toString();
    } //colorful version of properties(). Makes it a tad more readable, especially for big netlists.

    //these methods are used by the constructor to inherit the properties of another netlist.
    public HashMap<String,Boolean> allNets() {
        return nets;
    } //returns all nets with values
    public HashMap<String,netType> allNetProperties() {
        return netTypes;
    } //returns all nets with assigned netTypes
    public HashMap<String,NativeBlock> allBlocks() {
        return blockRegistry;
    } //returns the entire block registry
    public HashMap<String,HashSet<String>> allWireProperties() {
        return wires;
    } //returns the properties of all wires.

    //private methods
    private void setNets(HashSet<String> toSet, boolean value) {
        for(String net : toSet) {
            nets.replace(net,value);
        }
    } //set a bunch of nets to a certain value
    private void setNet(String net, boolean value) {
        nets.replace(net,value);
    } //set a single net to a certain value
    private void updateLocal(String netOrBlockName) {
        String blockName ="";
        if(netOrBlockName.split("_").length > 2) {
            //there's underscores setIn the block name itself! oh goodness!
            //a net name always looks like this: <blockname>_<setIn/readOut>_<someOtherThings>
            String[] a = netOrBlockName.split("_");
            for(int i = 0; i < a.length - 2; i++) {
                blockName += a[i] + "_";
            }
            blockName = blockName.substring(0,blockName.length()-1);
        }
        else {
            blockName = netOrBlockName.split("_")[0]; //whatever is before the first underscore is the block name
        }
        //if we're dealing with a simulator net, set all the input nets attached to it equal to the input net
        if(netTypes.get(netOrBlockName) == SIMULATOR_IN) {
            for(String wireNames : wires.keySet()) {
                if(wires.get(wireNames).contains(netOrBlockName)) {
                    //we've now found a wire that is attached to our simulator net.
                    for(String wireNets : wires.get(wireNames)) {
                        if(netTypes.get(wireNets) == INPUT) {
                            //set all input nets on the wire attached to the simulator net equal to the simulator net's value.
                            nets.replace(wireNets,nets.get(netOrBlockName));
                        }
                    }
                }
            }
        }

        //if we're dealing with a block, update the inputs to the block
        if(blockRegistry.containsKey(blockName)) {
            //get input nets that are part of the block
            for (String blockInNets : blockRegistry.get(blockName).getNets(INPUT).keySet()) {
                //blockInNets now goes through all the inputs for the given block.
                //set the block's inputs to whatever the nets hashmap says they should be
                //System.readOut.println("setIn updatelocal: blockname: " + blockName + ", net " + blockInNets + ", INPUT value of " + nets.get(blockInNets));
                blockRegistry.get(blockName).setIn(blockInNets, nets.get(blockInNets));
            }
            //now, update the nets hashmap with the new outputs from the block
            for(String blockOutNets : blockRegistry.get(blockName).getNets(OUTPUT).keySet()) {
                nets.replace(blockOutNets, blockRegistry.get(blockName).readOut(blockOutNets));
                //System.readOut.println("setIn updatelocal: blockname: " + blockName + ", net " + blockOutNets + ", OUTPUT value of " + nets.get(blockOutNets) + AnsiCodes.ANSI_RED + " CALCULATION RETURNS " + blockRegistry.get(blockName).readOut(blockOutNets) + AnsiCodes.ANSI_RESET);
            }
        }
    }   //interfaces with the block objects. this is the only place where the netlist's hashset actually changes based on the behavior of a gate.
    private int longestNetName() {
        int j = 0;
        for(String n : nets.keySet()) {
            if(n.length() > j) j = n.length();
        }
        return j;
    }
    private int longestWireName() {
        int j = 0;
        for(String n : wires.keySet()) {
            if(n.length() > j) j = n.length();
        }
        return j;
    }
    private int longestBlockName() {
        int j = 0;
        for(String n : blockRegistry.keySet()) {
            if(n.length() > j) j = n.length();
        }
        return j;
    }

    public enum netType {
        INPUT,
        OUTPUT,
        INOUT,
        SIMULATOR_IN,
        SIMULATOR_OUT
    }
}
