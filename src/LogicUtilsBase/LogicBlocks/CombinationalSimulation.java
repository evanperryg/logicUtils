package LogicUtilsBase.LogicBlocks;

import LogicUtilsBase.Algorithms.QuineMcCluskey;
import LogicUtilsBase.etc.StringTable;

import java.util.*;

import static java.lang.Math.PI;
import static java.lang.Math.pow;

/**
 * Created by evan on 4/15/2017.
 * Inherits a netlist and adds SIMULATOR_IN and SIMULATOR_OUT nets, which it uses to track the behavior
 * of the nets. Can take user inputted properties to run a behvaioral simulation, and, when allowed based on the
 * properties of the given netlist, can return an array of "true" minterms to be processed by the QuineMcCluskey
 * class.
 *
 * The trick here is that the user never actually sees the name of the simulation nets- they only see the wires
 * and their values. This makes reduces the complexity of making a simulation for the end user.
 */
public class CombinationalSimulation {
    private char[] termChars = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    private final NetList netList;
    private final String simName;
    private final LinkedHashMap<String,String> simInNames = new LinkedHashMap<>();  //key is wireName, value is netName
    private final LinkedHashMap<String,String> simOutNames = new LinkedHashMap<>(); //they're linked so the user can enter simulation values setIn the same order they entered the nets
    private HashMap<String,List<String>> minterms = new HashMap<>();

    public CombinationalSimulation(NetList netList) {
        this.netList = netList;
        this.simName = (netList.name() + "-simulation");

    }

    public CombinationalSimulation(NetList netList,String name) {
        this.netList = netList;
        this.simName = name;

    }

    public String addSimulationNet(NetList.netType nType, String wireName) {
        String newNetName;
        if(!netList.allWireNames().contains(wireName)) return ("CombinationalSimulation for NetList (" + netList.name() + "): net of type (" + nType +") was not added. Wire (" + wireName + ") does not exist.");
        switch(nType) {
            case SIMULATOR_IN:
                newNetName = String.valueOf(simName+"_out_"+simInNames.size());
                simInNames.put(wireName,newNetName);
                netList.addNet(newNetName);
                netList.addNetType(newNetName, NetList.netType.SIMULATOR_IN);
                break;
            case SIMULATOR_OUT:
                newNetName = String.valueOf(simName+"_in_"+simOutNames.size());
                simOutNames.put(wireName,newNetName);
                netList.addNet(newNetName);
                netList.addNetType(newNetName, NetList.netType.SIMULATOR_OUT);
                break;
            default:
                return ("CombinationalSimulation for NetList (" + netList.name() + "): net of type (" + nType +") was not added to wire (" + wireName + "). Simulations may not add non-simulation nets.");
        }
        netList.addWireConnection(wireName,newNetName);
        minterms.put(wireName,new ArrayList<>());
        return ("CombinationalSimulation for NetList (" + netList.name() + "): CombinationalSimulation net of type (" + nType + ") added to wire (" + wireName + ").");
    }

    public void removeSimulationNet(String wireName) {
        if(simInNames.keySet().contains(wireName)) {
            netList.removeWireConnection(wireName, simInNames.get(wireName));
            netList.removeNet(simInNames.get(wireName));
            simInNames.remove(wireName);
        }
        if(simOutNames.keySet().contains(wireName)) {
            netList.removeWireConnection(wireName, simOutNames.get(wireName));
            netList.removeNet(simOutNames.get(wireName));
            simOutNames.remove(wireName);
        }
        minterms.remove(wireName);
    }

    //sets a single simulation input
    public void setSimulationInput(String wireName, boolean value) {
        netList.setSimNet(simInNames.get(wireName),value);
    }

    //reads a single simulation output
    public Boolean readSimulationOutput(String wireName) {
        try {
            netList.updateAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(getWireType(wireName).equals("input")) return netList.valueOfSimNet(simInNames.get(wireName));
        else if(getWireType(wireName).equals("output")) return netList.valueOfSimNet(simOutNames.get(wireName));
        else return null;
    }

    //set all simulation inputs, get all simulation outputs simultaneously. This method should be repeated
    public LinkedHashMap<String,Boolean> combinationalSimulation(boolean... simInValues) {
        LinkedHashMap<String,Boolean> returnMap = new LinkedHashMap<>();
        if(simInValues.length != simInNames.size()) {
            return returnMap;
        }
        int i = 0;
        for(String n : simInNames.values()) { //values are netNames
            netList.setSimNet(n,simInValues[i]);
            i++;
        }
        try {
            netList.updateAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        for(String n : simOutNames.values()) { //keys are wire names
            returnMap.put(n,netList.valueOfSimNet(n));
        }
        return returnMap;
    }  //linked hashmap of the wires we wanted to read the output for
    private String combinationalSimulationString(boolean showHeader, boolean... simInValues) {
        int[] columns = new int[simInNames.size() + simOutNames.size()];
        Arrays.fill(columns,9);
        columns[simInNames.size()-1] = 12;
        StringTable st = new StringTable(' ',columns);
        String[] colNames = new String[columns.length];
        String returnString = "";
        if(showHeader) returnString = (truthTableHeader());

        if(simInValues.length != simInNames.size()) {
            return ("CombinationalSimulation for NetList (" + netList.name() + "): Combinational simulation failed. (count of given input values not equal to number of SIMULATOR_IN nets");
        }
        int i = 0;
        StringBuilder m = new StringBuilder();
        for(String n : simInNames.keySet()) {
            netList.setSimNet(simInNames.get(n),simInValues[i]);
            colNames[i] = String.valueOf(simInValues[i]);
            m.append(boolToChar(simInValues[i]));
            i++;
        }
        try {
            netList.updateAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        for(String n : simOutNames.keySet()) {
            colNames[i] = String.valueOf(netList.valueOfSimNet(simOutNames.get(n)));
            if(colNames[i].equals("true")) minterms.get(n).add(m.toString());
            i++;
        }
        return returnString.concat(st.makeTable(colNames));
    }   //get one line of a truth table
    public String truthTable() {
        String app = (truthTableHeader() + "\n");
        for(int i = 0; i < pow(2,simInNames.size()); i++) {
            app = app.concat(combinationalSimulationString(false,intToBoolArr(simInNames.size(),i))+"\n");
        }
        return app;
    }
    public String generateFunction(String wireName) {
        QuineMcCluskey q = new QuineMcCluskey(minterms.get(wireName),Arrays.copyOfRange(termChars,0,simInNames.size()));
        q.simplify();
        String exp = q.convertToExpression();
        if(exp.equals("1")) return "1";
        if(exp.equals("0")) return "0";

        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < exp.length(); i++) {
            switch (exp.charAt(i)) {
                case '+':
                case '\'':
                case '(':
                case ')':
                    sb.append(exp.charAt(i));
                    break;
                default:
                    int j = 0;
                    for(String n : simInNames.keySet()) {
                        if(termChars[j] == exp.charAt(i)) sb.append("[" + n + "]");
                        j++;
                    }
            }
        }
        return sb.toString();
    }

    public String name() { return simName; }
    public String netlistName() { return netList.name(); }
    public String getWireType(String wireName) {
        for(String j : simInNames.keySet()) {
            if(j.equals(wireName)) return "input";
        }
        for(String j : simOutNames.keySet()) {
            if(j.equals(wireName)) return "output";
        }
        return "null";
    }

    public ArrayList<String> wireNames() {
        ArrayList<String> foo = new ArrayList<>();
        foo.addAll(simInNames.keySet());
        foo.addAll(simOutNames.keySet());
        return foo;
    }

    private boolean[] intToBoolArr(int places, int val) {
        boolean[] out = new boolean[places];
        for(int i = places - 1; i >= 0; i--) {
            if(val >= pow(2,i)) {
                //System.readOut.println(val + " >= " + pow(2,i));
                out[out.length-i-1] = true;
                val -= pow(2,i);
            }
            else if(val < pow(2,i)) {
                //System.readOut.println(val + " < " + pow(2,i));
                out[out.length-i-1] = false;
            }
        }
        return out;
    }
    private String truthTableHeader() {
        int[] columns = new int[simInNames.size() + simOutNames.size()];
        Arrays.fill(columns,9);
        columns[simInNames.size()-1] = 12;
        StringTable st = new StringTable('-',columns);

        String[] colNames = new String[columns.length];
        int i = 0;
        for(String t : simInNames.keySet()) {
            colNames[i] = t;
            i++;
        }
        for(String t : simOutNames.keySet()) {
            colNames[i] = t;
            i++;
        }
        return st.makeTable(colNames);
    }
    private char boolToChar(Boolean b) {
        if(b) return '1';
        else return '0';
    }
}
