package LogicUtilsBase.LogicBlocks;

import java.util.*;

import static LogicUtilsBase.LogicBlocks.Block.blockType.NATIVE;
import static LogicUtilsBase.LogicBlocks.NetList.netType.*;

/**
 * Created by evan on 4/13/2017.
 *
 * The Block class implements NativeBlock interface, which is the general interface for all blocks in LogicTools.
 * Specifically, Block.java handles all basic logic gates, latches, flip-flops and blackboxes.
 */
public class Block implements NativeBlock {
    public enum blockType {
        BASIC,
        BLACKBOX,
        NATIVE
    }

    private final blockType bType;
    private basicBoxes preBox;
    private blackBox blkBox;

    private final HashMap<String,Boolean> inPins = new HashMap<>();
    private final HashMap<String,Boolean> outPins = new HashMap<>();
    //private HashSet<String> inOutPins = new HashSet<>(); NYI

    private boolean defined = false;
    private String bName;

    //general constructor.
    public Block(blockType bType) {
        this.bType = bType;
    }
    //for basicBoxes' basic gates
    public Block(blockType bType, basicBoxes.Box e, String newBlockName, int numOfInputs) {
        this.bType = bType;
        bName = newBlockName;
        if(defined) throw new NullPointerException("block has already been defined.");
        //HashMap<String,Boolean> ins = new HashMap<>();
        for(int i=0; i < numOfInputs; i++) {
            inPins.put(newBlockName + "_in_" + i,false);
        }
        outPins.put(newBlockName + "_out_0",false);
        switch(bType) {
            case BASIC:
                preBox = new basicBoxes(e,inPins,outPins,newBlockName);
                defined = true;
                break;
            default:
                throw new NullPointerException("incorrect syntax for defining block of type " + bType);
        }
    }
    //for basicBoxes latches and flip flops
    public Block(blockType bType, basicBoxes.Box e, String newBlockName, boolean preQ) {
        this.bType = bType;
        bName = newBlockName;
        if(defined) throw new NullPointerException("block has already been defined.");
        switch(e) {
            case SRNORLATCH:
            case SRNANDLATCH:
                inPins.put(newBlockName+"_in_S",false);
                inPins.put(newBlockName+"_in_R",false);
                outPins.put(newBlockName+"_out_Q",false);
                outPins.put(newBlockName+"_out_Qn",false);
                break;
            case SRNORENANDLATCH:
            case SRNORENNANDLATCH:
            case SRNANDENANDLATCH:
            case SRNANDENNANDLATCH:
                inPins.put(newBlockName+"_in_S",false);
                inPins.put(newBlockName+"_in_R",false);
                inPins.put(newBlockName+"_in_En",false);
                outPins.put(newBlockName+"_out_Q",false);
                outPins.put(newBlockName+"_out_Qn",false);
                break;
            case DENHILATCH:
                inPins.put(newBlockName+"_in_D",false);
                inPins.put(newBlockName+"_in_En",false);
                outPins.put(newBlockName+"_out_Q",false);
                outPins.put(newBlockName+"_out_Qn",false);
                break;
            case JKENHILATCH:
                inPins.put(newBlockName+"_in_J",false);
                inPins.put(newBlockName+"_in_K",false);
                inPins.put(newBlockName+"_in_En",false);
                outPins.put(newBlockName+"_out_Q",false);
                outPins.put(newBlockName+"_out_Qn",false);
                break;
            case DRISINGEDGEFF:
            case DFALLINGEDGEFF:
                inPins.put(newBlockName+"_in_D",false);
                inPins.put(newBlockName+"_in_Clk",false);
                outPins.put(newBlockName+"_out_Q",false);
                outPins.put(newBlockName+"_out_Qn",false);
                break;
            case JKRISINGEDGEFF:
            case JKFALLINGEDGEFF:
                inPins.put(newBlockName+"_in_J",false);
                inPins.put(newBlockName+"_in_K",false);
                inPins.put(newBlockName+"_in_Clk",false);
                outPins.put(newBlockName+"_out_Q",false);
                outPins.put(newBlockName+"_out_Qn",false);
                break;
            default:
                throw new NullPointerException("invalid block name or incorrect instantiation.");
        }

        switch(bType) {
            case BASIC:
                preBox = new basicBoxes(e,inPins,outPins,newBlockName,preQ);
                defined = true;
                break;
            default:
                throw new NullPointerException("incorrect syntax for defining block of type " + bType);
        }
    }
    //for blackboxes
    public Block(blockType bType, String newBlockName, HashMap<String,Character> inputHash, HashMap<String,Character> outputHash, HashMap<Character,String> setStatements) {
        this.bType = bType;
        bName = newBlockName;
        if(defined) throw new NullPointerException("block has already been defined.");
        for(String s : inputHash.keySet()) {
            inPins.put(newBlockName + "_" + s,false);
        }
        for(String s : outputHash.keySet()) {
            outPins.put(newBlockName + "_" + s,false);
        }
        switch(bType) {
            case BLACKBOX:
                blkBox = new blackBox(inputHash, outputHash, setStatements);
                defined = true;
                break;
            default:
                throw new NullPointerException("incorrect syntax for defining block of type " + bType);
        }
    }
    public Block(blockType bType, String newBlockName, String[] inputNames, String[] outputNames, String[] setStatements) {
        this.bType = bType;
        bName = newBlockName;
        if(defined) throw new NullPointerException("block has already been defined.");
        for(String in : Arrays.asList(inputNames)) {
            inPins.put(newBlockName + "_" + in,false);
        }
        for(String out : Arrays.asList(outputNames)) {
            outPins.put(newBlockName + "_" + out,false);
        }
        switch(bType) {
            case BLACKBOX:
                blkBox = new blackBox(inputNames, outputNames, setStatements);
                defined = true;
                break;
            default:
                throw new NullPointerException("incorrect syntax for defining block of type " + bType);
        }
    }

    public boolean readOut(String pinName) {
        switch(bType) {
            case BLACKBOX:
                return blkBox.out(pinName);
            case BASIC:
                return preBox.out(pinName);
            default:
                throw new NullPointerException("No block.readOut defined for blockType enum " + bType);
        }
    }
    public void setIn(String pinName, boolean j) {
        switch(this.bType) {
            case BASIC:
                preBox.in(pinName, j);
                break;
            case BLACKBOX:
                blkBox.in(pinName, j);
                break;
            default:
                throw new NullPointerException("No block.setIn defined for blockType enum " + bType);
        }
    }
    public boolean readIn(String pinName) {
        switch(bType) {
            case BLACKBOX:
                return blkBox.inRead(pinName);
            case BASIC:
                return preBox.inRead(pinName);
            default:
                throw new NullPointerException("No block.readIn defined for blockType enum " + bType);
        }
    }
    public NetList.netType getType(String pinName) {
        switch(bType) {
            case BLACKBOX:
                if(blkBox.netType(pinName)) return OUTPUT;
                else return INPUT;
            case BASIC:
                if(preBox.netType(pinName)) return OUTPUT;
                else return INPUT;
            default:
                throw new NullPointerException("No block.readOut defined for blockType enum " + bType);
        }
    }

    public void setNetType(String netName, NetList.netType type){
        try {
            throw new Exception("setNetType is not valid for Block");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public HashMap<String,Boolean> getNets(NetList.netType netType) {
        switch(netType) {
            case INPUT:
                return inPins;
            case OUTPUT:
                return outPins;
            case INOUT:
                //NYI
                break;
        }
        return new HashMap<>(); //return an empty set if a set doesn't exist
    }
    public String blockName() {return bName;}
    public blockType getBlockType() {return this.bType; }
    public basicBoxes.Box getBasicBoxesType() {
        if(this.bType == blockType.BASIC) {
            return preBox.basicBoxType();
        }
        else return null;
    }
    public blackBox getBlkBoxObject() { return blkBox; }
    public List<String> getNetNames() {
        Set<String> ins = inPins.keySet();
        Set<String> outs = outPins.keySet();
        List<String> allPins = new ArrayList<>();
        allPins.addAll(ins);
        allPins.addAll(outs);
        return allPins;
    }

}
