package LogicUtilsBase.LogicBlocks.MSI;

/**
 * Created by evan on 5/2/2017. Some MSI blocks implement the ValuedBlock interface, which allows a block to return a
 * value relating to its output. For example, the Hex-to-7-Segment decoder can return the character that is currently
 * outputting.
 */
public interface ValuedBlock {
    Object value();
}
