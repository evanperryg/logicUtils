package LogicUtilsBase.LogicBlocks.MSI;

import LogicUtilsBase.LogicBlocks.NativeBlock;
import LogicUtilsBase.LogicBlocks.NetList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by evan on 5/2/2017. Class for 7-segment display block. Converts any Hexadecimal input to 7-segment output.
 *
 */
public class SevenSegment implements NativeBlock, ValuedBlock {
    private String name;

    private HashMap<String,Boolean> ins = new HashMap<>();
    private HashMap<String,Boolean> outs = new HashMap<>();

    public SevenSegment(String blockName) {
        name = blockName;
        ins.put(blockName + "_in_a",false);
        ins.put(blockName + "_in_b",false);
        ins.put(blockName + "_in_c",false);
        ins.put(blockName + "_in_d",false);
        outs.put(blockName + "_out_a",false);
        outs.put(blockName + "_out_b",false);
        outs.put(blockName + "_out_c",false);
        outs.put(blockName + "_out_d",false);
        outs.put(blockName + "_out_e",false);
        outs.put(blockName + "_out_f",false);
        outs.put(blockName + "_out_g",false);
    }

    public SevenSegment(String blockName, List<String> inNets, List<String> outNets) {
        name = blockName;
        for(String i : inNets) {
            ins.put(i,false);
        }
        for(String i : outNets) {
            outs.put(i,false);
        }
    }

    //10 = A, 11 = B...
    public Character value() {
        if(intValue() < 10) {
            return (char) (intValue() + 48);
        }
        else {
            return (char) (intValue() + 55);
        }
    }

    public boolean readOut(String outName) {
        //this ensures that the nets can be addressed either as <blockName>_in_0 or  in_0
        if(!outName.startsWith(name)) {
            outName = name + "_" + outName;
        }
        //update all outputs, return the requested one
        switch(intValue()) {
            case 0:
                outs.replace(name + "_out_a",true);
                outs.replace(name + "_out_b",true);
                outs.replace(name + "_out_c",true);
                outs.replace(name + "_out_d",true);
                outs.replace(name + "_out_e",true);
                outs.replace(name + "_out_f",true);
                outs.replace(name + "_out_g",false);
                break;
            case 1:
                outs.replace(name + "_out_a",false);
                outs.replace(name + "_out_b",true);
                outs.replace(name + "_out_c",true);
                outs.replace(name + "_out_d",false);
                outs.replace(name + "_out_e",false);
                outs.replace(name + "_out_f",false);
                outs.replace(name + "_out_g",false);
                break;
            case 2:
                outs.replace(name + "_out_a",true);
                outs.replace(name + "_out_b",true);
                outs.replace(name + "_out_c",false);
                outs.replace(name + "_out_d",true);
                outs.replace(name + "_out_e",true);
                outs.replace(name + "_out_f",false);
                outs.replace(name + "_out_g",true);
                break;
            case 3:
                outs.replace(name + "_out_a",true);
                outs.replace(name + "_out_b",true);
                outs.replace(name + "_out_c",true);
                outs.replace(name + "_out_d",true);
                outs.replace(name + "_out_e",false);
                outs.replace(name + "_out_f",false);
                outs.replace(name + "_out_g",true);
                break;
            case 4:
                outs.replace(name + "_out_a",false);
                outs.replace(name + "_out_b",true);
                outs.replace(name + "_out_c",true);
                outs.replace(name + "_out_d",false);
                outs.replace(name + "_out_e",false);
                outs.replace(name + "_out_f",true);
                outs.replace(name + "_out_g",true);
                break;
            case 5:
                outs.replace(name + "_out_a",true);
                outs.replace(name + "_out_b",true);
                outs.replace(name + "_out_c",true);
                outs.replace(name + "_out_d",true);
                outs.replace(name + "_out_e",false);
                outs.replace(name + "_out_f",true);
                outs.replace(name + "_out_g",true);
                break;
            case 6:
                outs.replace(name + "_out_a",true);
                outs.replace(name + "_out_b",false);
                outs.replace(name + "_out_c",true);
                outs.replace(name + "_out_d",true);
                outs.replace(name + "_out_e",true);
                outs.replace(name + "_out_f",true);
                outs.replace(name + "_out_g",true);
                break;
            case 7:
                outs.replace(name + "_out_a",true);
                outs.replace(name + "_out_b",true);
                outs.replace(name + "_out_c",true);
                outs.replace(name + "_out_d",false);
                outs.replace(name + "_out_e",false);
                outs.replace(name + "_out_f",false);
                outs.replace(name + "_out_g",false);
                break;
            case 8:
                outs.replace(name + "_out_a",true);
                outs.replace(name + "_out_b",true);
                outs.replace(name + "_out_c",true);
                outs.replace(name + "_out_d",true);
                outs.replace(name + "_out_e",true);
                outs.replace(name + "_out_f",true);
                outs.replace(name + "_out_g",true);
                break;
            case 9:
                outs.replace(name + "_out_a",true);
                outs.replace(name + "_out_b",true);
                outs.replace(name + "_out_c",true);
                outs.replace(name + "_out_d",true);
                outs.replace(name + "_out_e",false);
                outs.replace(name + "_out_f",true);
                outs.replace(name + "_out_g",true);
                break;
            case 10:
                outs.replace(name + "_out_a",true);
                outs.replace(name + "_out_b",true);
                outs.replace(name + "_out_c",true);
                outs.replace(name + "_out_d",false);
                outs.replace(name + "_out_e",true);
                outs.replace(name + "_out_f",true);
                outs.replace(name + "_out_g",true);
                break;
            case 11:
                outs.replace(name + "_out_a",false);
                outs.replace(name + "_out_b",false);
                outs.replace(name + "_out_c",true);
                outs.replace(name + "_out_d",true);
                outs.replace(name + "_out_e",true);
                outs.replace(name + "_out_f",true);
                outs.replace(name + "_out_g",true);
                break;
            case 12:
                outs.replace(name + "_out_a",true);
                outs.replace(name + "_out_b",false);
                outs.replace(name + "_out_c",false);
                outs.replace(name + "_out_d",true);
                outs.replace(name + "_out_e",true);
                outs.replace(name + "_out_f",true);
                outs.replace(name + "_out_g",false);
                break;
            case 13:
                outs.replace(name + "_out_a",false);
                outs.replace(name + "_out_b",true);
                outs.replace(name + "_out_c",true);
                outs.replace(name + "_out_d",true);
                outs.replace(name + "_out_e",true);
                outs.replace(name + "_out_f",false);
                outs.replace(name + "_out_g",true);
                break;
            case 14:
                outs.replace(name + "_out_a",true);
                outs.replace(name + "_out_b",false);
                outs.replace(name + "_out_c",false);
                outs.replace(name + "_out_d",true);
                outs.replace(name + "_out_e",true);
                outs.replace(name + "_out_f",true);
                outs.replace(name + "_out_g",true);
                break;
            case 15:
                outs.replace(name + "_out_a",true);
                outs.replace(name + "_out_b",false);
                outs.replace(name + "_out_c",false);
                outs.replace(name + "_out_d",false);
                outs.replace(name + "_out_e",true);
                outs.replace(name + "_out_f",true);
                outs.replace(name + "_out_g",true);
                break;
        }
        return outs.get(outName);
    }

    public boolean readIn(String inName) {
        if(!inName.startsWith(name)) {
            inName = name + "_" + inName;
        }
        return ins.get(inName);
    }

    public void setNetType(String netName, NetList.netType type) {
        //nets don't have settable types for this block.
    }

    public void setIn(String inName, boolean value) {
        if(!inName.startsWith(name)) {
            inName = name + "_" + inName;
        }
        if(ins.replace(inName,value) == null) {
            try {
                throw new Exception("SevenSegment: invalid input name specified for setIn");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public NetList.netType getType(String netName) {
        if(outs.containsKey(netName)) return NetList.netType.OUTPUT;
        else if(ins.containsKey(netName)) return NetList.netType.INPUT;
        else return null;
    }

    public HashMap<String, Boolean> getNets(NetList.netType netType) {
        switch(netType) {
            case INPUT:
                return ins;
            case OUTPUT:
                return outs;
            default:
                return new HashMap<>();
        }
    }

    public List<String> getNetNames() {
        List<String> out = new ArrayList<>();
        out.addAll(ins.keySet());
        out.addAll(outs.keySet());
        return out;
    }

    public String blockName() {
        return name;
    }

    public Object getBlockType() {
        return "7SegmentDisplayDriver";
    }

    private int intValue() {
        int out = 0;
        if(ins.get(name + "_in_a")) out++;
        if(ins.get(name + "_in_b")) out+=2;
        if(ins.get(name + "_in_c")) out+=4;
        if(ins.get(name + "_in_d")) out+=8;
        return out;
    }

}
