package LogicUtilsBase.LogicBlocks.MSI;

import LogicUtilsBase.LogicBlocks.NativeBlock;
import LogicUtilsBase.LogicBlocks.NetList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static java.lang.Math.pow;

/**
 * Created by evan on 5/3/2017. This class allows the creation of various sizes of multiplexers. NativeBlock objects that
 * aren't part of the Block class have to have two different constructors. The first one, the "automatic" constructor,
 * is given only the block's name, sometimes with an integer condition or two. This constructor will assign the inputs
 * and outputs by itself. The other constructor takes lists of nets, with each list corresponding to a different type of
 * net, and these nets are forcibly assigned to the block. This second constructor is ONLY EVER USED BY THE FILE MANAGEMENT
 * CLASSES, for the sole purpose of massively simplifying the file opening process.
 */
public class Multiplexer implements NativeBlock {
    private String name;
    private int contIns;

    private HashMap<String,Boolean> ins = new HashMap<>();
    private HashMap<String,Boolean> outs = new HashMap<>();

    public Multiplexer(String blockName, int S) {
        name = blockName;
        contIns = S;
        for(int i = 0; i < S; i++) {
            ins.put(blockName + "_in_S" + String.format("%02d",i),false);
        }
        for(int i = 0; i < pow(2,S); i++) {
            ins.put(blockName + "_in_I" + String.format("%02d",i),false);
        }
        outs.put(blockName + "_out",false);
    }

    public Multiplexer(String blockName, List<String> inNets, List<String> outNets) {
        name = blockName;
        for(String i : inNets) {
            ins.put(i,false);
        }
        for(String i : outNets) {
            outs.put(i,false);
        }
    }

    public boolean readOut(String outName) {
        if(!outName.startsWith(name)) {
            outName = name + "_" + outName;
        }
        outs.put(name + "_out",ins.get(name + "_in_I" + String.format("%02d",pick())));
        return outs.get(outName);
    }

    public boolean readIn(String inName) {
        if(!inName.startsWith(name)) {
            inName = name + "_" + inName;
        }
        return ins.get(inName);
    }

    public void setNetType(String netName, NetList.netType type) {
        //none of this block's nets are settable
    }

    public void setIn(String inName, boolean value) {
        if(!inName.startsWith(name)) {
            inName = name + "_" + inName;
        }
        ins.replace(inName,value);
    }

    public NetList.netType getType(String netName) {
        if(outs.containsKey(netName)) return NetList.netType.OUTPUT;
        else if(ins.containsKey(netName)) return NetList.netType.INPUT;
        else return null;
    }

    public HashMap<String, Boolean> getNets(NetList.netType netType) {
        switch(netType) {
            case INPUT:
                return ins;
            case OUTPUT:
                return outs;
            default:
                return new HashMap<>();
        }
    }

    public List<String> getNetNames() {
        List<String> out = new ArrayList<>();
        out.addAll(ins.keySet());
        out.addAll(outs.keySet());
        return out;
    }

    public String blockName() {
        return name;
    }

    public Object getBlockType() {
        return "Multiplexer";
    }

    private int pick() {
        int out = 0;
        for(int i = 0; i < contIns; i++) {
            if(ins.get(name+"_in_S"+String.format("%02d",i))) out += pow(2,i);
        }
        return out;
    }
}
