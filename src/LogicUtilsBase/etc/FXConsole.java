package LogicUtilsBase.etc;

import java.util.ArrayList;
import java.util.List;

import static LogicUtilsBase.etc.FXConsole.l1command.*;

/**
 * Created by evan on 4/18/2017. This class performs some of the core parsing of commands entered into the LogicTools
 * console. The guide for the commands can be seen here, or setIn the String at the bottom of the class:
 *
 */
public class FXConsole {
    private String[] inStr;
    private String inLine;

    private List<String> netListNames = new ArrayList<>();
    private List<String> simulationNames = new ArrayList<>();
    private  List<String> blackboxNames = new ArrayList<>();

    public FXConsole(){

    }

    public enum l1command {
        HELP,
        NEW_NETLIST,
        ADD_BLOCK,
        REM_BLOCK,
        ADD_WIRE,
        REM_WIRE,
        ADD_WIRENET,
        REM_WIRENET,
        NEW_BBX,
        SIM_ADD_IN,
        SIM_ADD_OUT,
        SIM_REM,
        REM_SIMNET,
        NEW_COMBINATIONAL_SIM,
        NEW_SEQUENTIAL_SIM,
        SIM_RUN,
        SIM_SET,
        SIM_TRUTHTABLE,
        SIM_FUNCTION,
        CLEAR,
        DELETE,
        DIARY_ON,
        DIARY_OFF,
        ERROR
    }

    public void in(String inLine) {
        this.inLine = inLine;
        this.inStr = inLine.split(" ");
    }

    public l1command consoleOut() {
        if(inStr[0].equals("new")) {
            switch(inStr[1]) {
                case "netlist":
                case "nl":
                    if(inStr.length == 3) {
                        netListNames.add(inStr[2]);
                        System.out.println(inStr[2]);
                        return NEW_NETLIST;
                    }
                    else return ERROR;
                case "blackbox":
                case "bbx":
                    blackboxNames.add(inStr[1]);
                    return NEW_BBX;
                case "simulation":
                case "sim":
                    if(inStr.length > 2) {
                        if (inStr[2].equals("-c")) {
                            simulationNames.add(inStr[2]);
                            return NEW_COMBINATIONAL_SIM;
                        }
                        else if (inStr[2].equals("-s")) {
                            simulationNames.add(inStr[2]);
                            return NEW_SEQUENTIAL_SIM;
                        }
                    }
                    return ERROR;
                default:
                    //configure the error message here
                    return ERROR;
            }
        }
        else if(inStr[0].equals("diary")) {
            switch(inStr[1]) {
                case "begin":
                    return DIARY_ON;
                case "end":
                    return DIARY_OFF;
            }
            return ERROR;
        }
        else if(simulationNames.contains(inStr[0])) {
            //we are manipulating a simulation
            switch(inStr[1]) {
                case "add":
                case "attach":
                    if(inStr[2].equals("-i")) return SIM_ADD_IN;
                    else if(inStr[2].equals("-o")) return SIM_ADD_OUT;
                    else return ERROR;
                case "remove":
                case "rem":
                    if(inStr.length == 2) return SIM_REM;
                    else if(inStr[2].equals("wire")) return REM_SIMNET;
                    return ERROR;
                case "run":
                    if(inStr.length == 2) return SIM_RUN;
                    else if(inStr[2].equals("tt")) return SIM_TRUTHTABLE;
                    else if(inStr[2].equals("func")) {
                        if(inStr.length > 3) return SIM_FUNCTION;
                    }
                    return ERROR;
                default:
                    return ERROR;
            }
        }
        else if(netListNames.contains(inStr[0])) {
            //we are manipulating a netlist
            switch(inStr[1]) {
                case "add":
                    if(inStr[2].equals("wire")) return ADD_WIRE;
                    else if(inStr[2].equals("block")) return ADD_BLOCK;
                    else if(inStr[2].equals("simulation") || inStr[2].equals("sim")) {
                        if(inStr[3].equals("-c")) return NEW_COMBINATIONAL_SIM;
                        else if(inStr[3].equals("-s")) return NEW_SEQUENTIAL_SIM;
                    }
                    else return ERROR;
                case "remove":
                case "rem":
                    if(inStr[2].equals("wire")) {
                        if(inStr.length > 4) return REM_WIRENET;
                        else if(inStr.length == 4) return REM_WIRE;
                    }
                    else if(inStr[2].equals("block")) return REM_BLOCK;
                    else return ERROR;
            }

        }
        else if(blackboxNames.contains(inStr[0])) {
            //we are manipulating a blackbox

        }
        else if(inStr[0].equals("help")) {
            return HELP;
        }

        return ERROR; //we should never get here unless the first-level command was invalid.
    }

    public String conditionAt(int i) throws ArrayIndexOutOfBoundsException { return inStr[i]; }

    public String[] conditionsFrom(int i) {
        String[] out = new String[inStr.length-i];
        System.arraycopy(inStr,i,out,0,out.length);
        return out;
    }

    public void addToNetLists(String name) {
        if(!netListNames.contains(name)) {
            netListNames.add(name);
        }
    }

    public void addToSims(String name) {
        System.out.println("foo!");
        if(!simulationNames.contains(name)) {
            simulationNames.add(name);
        }
    }

    public void addToBBX(String name) {
        if(!blackboxNames.contains(name)) {
            blackboxNames.add(name);
        }
    }

    public static String helpMessage =
                    "LogicTools r1705a Console Guide\n" +
                    //"type \"help <nameofcommand>\" for specific details about a command." +
                            "new             netlist     <netListName>\n" +
                            "                blackbox    <bbxName>\n" +
                            "--------------------------------------------------------------------\n" +
                            "diary           begin       <diaryFileName>\n" +
                            "                end\n" +
                            "--------------------------------------------------------------------\n" +
                            "<netListName>   add         wire        <wireName>    <netsToAttach>\n" +
                            "                            block\n" +
                            "                simulation  (-c/-s)     <simName>\n" +
                            "                remove      wire        <wireName>    <netsToDetach>\n" +
                            "                                        <wireName>\n" +
                            "--------------------------------------------------------------------\n" +
                            "<simName>       attach      (-i/-o)     <wireToAttach>\n" +
                            "                remove      wire        <wireToDetach>\n" +
                            "                set         <wireName>  <value>\n" +
                            "                run         tt\n" +
                            "                            func        <wireName>\n";
}
