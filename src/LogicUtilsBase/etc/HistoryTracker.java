package LogicUtilsBase.etc;

import sun.net.www.content.text.Generic;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by evan on 4/18/2017. This class is used to track history of things. Specify how far back it should go, and
 * pass it things to hold onto. When you need to check history, ask it and it'll spit it out.
 *
 * Element 0 in the private string is the oldest item, element n is the most recently added.
 */
public class HistoryTracker {
    String[] hist;

    public HistoryTracker(int len) {
        this.hist = new String[len];
        for(int i = 0; i < len; i++) {
            hist[i] = "";
        }
    }

    public void add(String item) {
        if(highestOpenElement() > 0) {
            hist[highestOpenElement()] = item;
        }
        else {
            shift(1);
            hist[hist.length-1] = item;
        }
    }

    public String get() {
        return hist[hist.length-1];
    }

    public String get(int i) {
        if(i < hist.length && i > -1) {
            return hist[hist.length - i - 1];
        }
        else return null;
    }

    public void removeNewest(int i) {
        hist[highestOpenElement()-1] = "";
    }

    private void shift(int j) {
        //j is the number of "spaces" we are going to move the array
        for(int i = 0; i < hist.length; i++) {
            if(i >= hist.length - j) {
                hist[i] = "";
            }
            else {
                hist[i] = hist[i + j];
            }
        }
    }

    private int highestOpenElement() {
        for(int i = 0; i < hist.length; i++) {
            if(hist[i].isEmpty()) return i;
        }
        return -1;
    }
}
