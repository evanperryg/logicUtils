package LogicUtilsBase.etc;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by evan on 4/13/2017.
 * Some nice little hashmap utilities
 *
 * subhash is basically the ubiquitious substr() function but for hashmaps.
 * This can be used to get the intersection of hashmaps A and B
 * <pre>
 * <code>
 * {@literal@}
 *      HashMap&lt;K,V&gt; c = new HashMap&lt;&gt;();
 *      c = subhash(b, a.keySet().toArray());
 * </code>
 * </pre>
 * Or, it can be used to get only the pairs corresponding to the given keys. pseudocode:
 *      .
 * <pre>
 * <code>
 * {@literal@}
 *      HashMap&lt;K,V&gt; c = new HashMap&lt;&gt;();
 *      K[] keysToKeep = {some,k,values}
 *      c = subhash(b, keysToKeep);
 * </code>
 * </pre>
 * hashswap returns a HashMap with keys corresponding to every unique value setIn the given
 * HashMap, and with values that are a set of values corresponding to each key. In other
 * words, it swaps the given HashMap without ignoring duplicates.
 **/
public class hashTools {

    public static <K,V> HashMap<K,V> subhash(HashMap<K,V> h, K... tocopy) {
        HashMap<K,V> b = new HashMap<>();
        for(K w : tocopy) {
            if(h.get(w) != null) {
                b.put(w, h.get(w));
            }
        }
        return b;
    }

    public static <K,V> HashMap<K,V> subhash(HashMap<K,V> h, Set<K> tocopy) {
        for(K w : tocopy) {
            if(!tocopy.contains(w)) {
                h.remove(w);
            }
        }
        return h;
    }

    public static <V> HashSet<V> subset(Set<V> h, Set<V> tocopy) {
        HashSet<V> out = new HashSet<>();
        for(V v : h) {
            if(tocopy.contains(v)) {
                out.add(v);
            }
        }
        return out;
    }

    public static <V> HashSet<V> excludeSetVals(Set<V> h, Set<V> toexclude) {
        HashSet<V> out = new HashSet<>(h);
        for(V v : h) {
            if(toexclude.contains(v)) {
                out.remove(v);
            }
        }
        return out;
    }

    public static <V> HashSet<V> excludeSetVals(Set<V> h, V... toexclude) {
        HashSet<V> out = new HashSet<>(h);
        for(V v : h) {
            for(V rem : toexclude) {
                if (rem == v) out.remove(v);
            }
        }
        return out;
    }

    public static <V,K> HashMap<V,Set<K>> hashswap(HashMap<K,V> h) {
        //determine how many unique V there are
        HashSet<V> setofV = new HashSet<>();
        for(K w : h.keySet()) {
            setofV.add(h.get(w));
        }
        //unique Vs count = setofV.size()

        HashMap<V,Set<K>> out = new HashMap<>();
        //then, add the new set to the output HashMap
        for(V v : setofV) {
            Set<K> subset = new HashSet<>();
            for(K k : h.keySet()) {
                //if the v for the given k equals whatever v setIn set setofV we are currently on
                //add k to the subset
                if(h.get(k) == v) {
                    subset.add(k);
                }
            }
            //now, put subset setIn hashmap readOut, mapping it to v
            out.put(v,subset);
        }
        return out;
    }

    public static <K,V> HashMap<K,V> excludeMapVals(HashMap<K,V> h, V... vToExclude) {
        for(V v : vToExclude) {
            //doing a simple remove only gets rid of one of the elements tied to this value
            h.values().removeAll(Collections.singleton(v));
        }
        return h;
    }
}
