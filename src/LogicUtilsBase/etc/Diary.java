package LogicUtilsBase.etc;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by evan on 4/19/2017. This class handles the opening, closing, and writing of diary files.
 */
public class Diary {
    private StringBuilder entries = new StringBuilder();
    private String filename;
    private boolean initialized = false;

    public Diary() {

    }

    public void make(String filename) {
        initialized = true;
        this.filename = filename;
    }

    public boolean isEnabled() {
        return initialized;
    }

    public void add(String newLine) {
        entries.append(newLine);
    }

    public boolean endsWithNewline() {
        return entries.length() <= 0 || entries.charAt(entries.length() - 1) == '\n';
    }

    public void close() {
        FileWriter fw = null;
        BufferedWriter bw = null;

        try {
            fw = new FileWriter(filename);
            bw = new BufferedWriter(fw);

            bw.write(entries.toString());

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null)
                    bw.close();
                if (fw != null)
                    fw.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        entries.setLength(0);
        entries.trimToSize();
        initialized = false;
    }

    public String getName() { return filename; }
}
