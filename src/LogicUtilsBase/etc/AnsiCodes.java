package LogicUtilsBase.etc;


/**
 * Created by evan on 4/4/2017.
 * ANSI codes neatly tied up into an interface. This is used to make some of the debugging stuff a bit easier
 * to read.
 */
public interface AnsiCodes {
    String ANSI_RESET = "\u001B[0m";

    String ANSI_BLACK = "\u001B[30m";
    String ANSI_RED = "\u001B[31m";
    String ANSI_GREEN = "\u001B[32m";
    String ANSI_YELLOW = "\u001B[33m";
    String ANSI_BLUE = "\u001B[34m";
    String ANSI_PURPLE = "\u001B[35m";
    String ANSI_CYAN = "\u001B[36m";
    String ANSI_WHITE = "\u001B[37m";

    String	BACKGROUND_BLACK	= "\u001B[40m";
    String	BACKGROUND_RED		= "\u001B[41m";
    String	BACKGROUND_GREEN	= "\u001B[42m";
    String	BACKGROUND_YELLOW	= "\u001B[43m";
    String	BACKGROUND_BLUE		= "\u001B[44m";
    String	BACKGROUND_MAGENTA	= "\u001B[45m";
    String	BACKGROUND_CYAN		= "\u001B[46m";
    String	BACKGROUND_WHITE	= "\u001B[47m";

    String	HIGH_INTENSITY		= "\u001B[1m";
    String	LOW_INTENSITY		= "\u001B[2m";

    String	ITALIC				= "\u001B[3m";
    String	UNDERLINE			= "\u001B[4m";
    String	BLINK				= "\u001B[5m";
}
