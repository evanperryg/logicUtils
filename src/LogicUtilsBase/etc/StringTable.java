package LogicUtilsBase.etc;

/**
 * Created by evan on 4/15/2017.
 * Enables the easy creation of many strings that share the same "column" width. The constructor takes widths for
 * each column, and the makeLine method will take strings for each column and concatenate them in such a way that
 * they follow the widths given in the constructor.
 *
 * If a string for a given column is longer than the column width, it gets "cut off" by the next string. The last
 * string will be cut off at the given columnLength if you use makeTable(), but if you use makeLine(), the entire
 * last string will be included.
 *
 * This is a brutally simple implementation that does just enough to serve my purposes. It is not capable of
 * adding dividers between columns, nor is it capable of spitting out an entire table in one go. It does exactly
 * what is needed for this application, nothing more, nothing less.
 */
public class StringTable {
    private final int[] columnLengths;
    private final char filler;
    //private int[] startColumnsAt;
    //private int strlen;
    public StringTable(char fillerChar, int... colWidth) {
        this.columnLengths = colWidth;
        this.filler = fillerChar;
        //this.strlen = IntStream.of(colWidth).sum();
//        this.startColumnsAt = new int[colWidth.length];
//        startColumnsAt[0] = 0;
//        for(int i = 1; i < columnLengths.length; i++) {
//            startColumnsAt[i] += columnLengths[i-1] + startColumnsAt[i-1];
//        }
    }

    public String makeTable(String... elements) {
        String outStr = "";
        for(int i = 0; i < columnLengths.length; i++) {
            //append whatever should be appended at this point.
            String thisColumnStr = "";
            if(elements[i].length() < columnLengths[i]) {
                StringBuilder thisColumnStrBuilder = new StringBuilder(elements[i]);
                while(thisColumnStrBuilder.length() < columnLengths[i]) {
                    thisColumnStrBuilder.append(filler);
                }
                thisColumnStr = thisColumnStrBuilder.toString();
            }
            else {
                thisColumnStr = thisColumnStr.concat(elements[i].substring(0,columnLengths[i]));
            }
            outStr = outStr.concat(thisColumnStr);
        }
        return outStr;
    }

    public String makeLine(String... elements) {
        String outStr = "";
        for(int i = 0; i < columnLengths.length-1; i++) {
            //append whatever should be appended at this point.
            String thisColumnStr = "";
            if(elements[i].length() < columnLengths[i]) {
                StringBuilder thisColumnStrBuilder = new StringBuilder(elements[i]);
                while(thisColumnStrBuilder.length() < columnLengths[i]) {
                    thisColumnStrBuilder.append(filler);
                }
                thisColumnStr = thisColumnStrBuilder.toString();
            }
            else {
                thisColumnStr = thisColumnStr.concat(elements[i].substring(0,columnLengths[i]));
            }
            outStr = outStr.concat(thisColumnStr);
        }
        outStr = outStr.concat(elements[columnLengths.length-1]);
        return outStr;
    }

}
