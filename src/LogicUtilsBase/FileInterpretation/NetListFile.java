package LogicUtilsBase.FileInterpretation;

import LogicUtilsBase.LogicBlocks.Block;
import LogicUtilsBase.LogicBlocks.MSI.Multiplexer;
import LogicUtilsBase.LogicBlocks.MSI.SevenSegment;
import LogicUtilsBase.LogicBlocks.NativeBlock;
import LogicUtilsBase.LogicBlocks.NetList;
import LogicUtilsBase.LogicBlocks.basicBoxes;
import LogicUtilsBase.etc.hashTools;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.BufferedWriter;
import java.io.FileWriter;

import java.io.IOException;
import java.util.*;

/**
 * Created by evan on 4/4/2017.
 * Read and write netlists to savefiles, and checks for some syntax errors setIn the file before returning a Netlist.
 */
public class NetListFile {
    private String filename;

    public NetListFile(){

    }
    public NetListFile(String filename) {
        if(!filename.contains(".nlf")) filename = filename.concat(".nlf");
        this.filename = filename;
    }

    public void write(NetList netList) {
        FileWriter fw = null;
        BufferedWriter bw = null;

        try {
            //it should be noted that this setup does not yet fully support saving blackboxes. It does not save all
            //the blackbox properties to the file, yet.

            //build the string that should go into the file
            StringBuilder sb = new StringBuilder();
            //now, create a list of "permitted" nets. That is, all nets except simulation nets. These nets will be
            //saved to the file, while the simulator nets will be left behind
            HashMap<String,NetList.netType> netsAllowed = hashTools.excludeMapVals(netList.allNetProperties(),NetList.netType.SIMULATOR_IN, NetList.netType.SIMULATOR_OUT);
            //start with the name of the netlist
            sb.append("NETLIST:").append(netList.name()).append("\n");
            //run the netlist through the blackbox file creator. if it returns a map of blackbox property files, make sure to retrieve those.
            BlackBoxFile bbx = new BlackBoxFile();
            HashMap<String,String> bbxAssociations = bbx.write(netList);
            if(!bbxAssociations.isEmpty()) {
                //if this is the case, bbx has written blackbox files associated with this netlist and has returned the filenames associated with each block.
                sb.append("BBXDEFINES:").append(bbxAssociations).append("\n");
            }
            //now add the blocks.
            for(String k : netList.allBlocks().keySet()) {
                sb.append("BLOCK:").append(k)
                        .append("=").append(netList.allBlocks().get(k).getBlockType())
                        .append("=").append(netList.blockSubProperty(k))
                        .append("=").append(netList.blockNets(k))
                        .append("\n");
            }
            //add the nets themselves, with values
            sb.append("NETVALS:").append(hashTools.subhash(netList.allNets(),netsAllowed.keySet()))
                    .append("\n");
            //add the net properties, excluding simulator nets
            sb.append("NETPROPERTIES:").append(netsAllowed)
                    .append("\n");
            //add the wire properties, again excluding simulator nets
            for(String k : netList.allWireProperties().keySet()) {
                sb.append("WIRE:").append(k)
                        .append("=").append(hashTools.subset(netList.allWireProperties().get(k),netsAllowed.keySet()))
                        .append("\n");
            }

            //now try to assemble the file writeout
            fw = new FileWriter(filename);
            bw = new BufferedWriter(fw);
            bw.write(sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null)
                    bw.close();
                if (fw != null)
                    fw.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }
    public static void write(NetList netList, String filename) {
        if(!filename.contains(".nlf")) filename = filename.concat(".nlf");

        FileWriter fw = null;
        BufferedWriter bw = null;

        try {
            //it should be noted that this setup does not yet fully support saving blackboxes. It does not save all
            //the blackbox properties to the file, yet.

            //build the string that should go into the file
            StringBuilder sb = new StringBuilder();
            //now, create a list of "permitted" nets. That is, all nets except simulation nets. These nets will be
            //saved to the file, while the simulator nets will be left behind
            HashMap<String,NetList.netType> netsAllowed = hashTools.excludeMapVals(netList.allNetProperties(),NetList.netType.SIMULATOR_IN, NetList.netType.SIMULATOR_OUT);
            //start with the name of the netlist
            sb.append("NETLIST:").append(netList.name()).append("\n");
            //run the netlist through the blackbox file creator. if it returns a map of blackbox property files, make sure to retrieve those.
            BlackBoxFile bbx = new BlackBoxFile();
            HashMap<String,String> bbxAssociations = bbx.write(netList);
            if(!bbxAssociations.isEmpty()) {
                //if this is the case, bbx has written blackbox files associated with this netlist and has returned the filenames associated with each block.
                sb.append("BBXDEFINES:").append(bbxAssociations).append("\n");
            }
            //now add the blocks.
            for(String k : netList.allBlocks().keySet()) {
                sb.append("BLOCK:").append(k)
                        .append(":").append(netList.allBlocks().get(k).getBlockType())
                        .append(":").append(netList.blockSubProperty(k))
                        .append(":").append(netList.blockNets(k))
                        .append("\n");
            }
            //add the net properties, excluding simulator nets
            sb.append("NETPROPERTIES:").append(netsAllowed)
                    .append("\n");
            //add the nets themselves, with values
            sb.append("NETVALS:").append(hashTools.subhash(netList.allNets(),netsAllowed.keySet()))
                    .append("\n");
            //add the wire properties, again excluding simulator nets
            for(String k : netList.allWireProperties().keySet()) {
                sb.append("WIRE:").append(k)
                        .append(":").append(hashTools.subset(netList.allWireProperties().get(k),netsAllowed.keySet()))
                        .append("\n");
            }

            //now try to assemble the file writeout
            fw = new FileWriter(filename);
            bw = new BufferedWriter(fw);
            bw.write(sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null)
                    bw.close();
                if (fw != null)
                    fw.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static NetList openNLF(String filename) {
        if(!filename.contains(".nlf")) filename = filename.concat(".nlf");
        //create variables for all the stuff we're gonna add to the NetList before returning it
        NetList nl = null;

        try(BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            while((line = br.readLine()) != null) {
                String[] splitLine = line.replaceAll("[ {}]","").split(":");
                switch (splitLine[0]) {
                    case "NETLIST":
                        nl = new NetList(splitLine[1]);
                        break;
                    case "BBXDEFINES":
                        String fname = splitLine[1].split("=")[1];
                        String bname = splitLine[1].split("=")[0];
                        Block bbx = BlackBoxFile.openBBX(fname);
                        if(!bbx.blockName().equals(bname)) {
                            System.out.println("WARNING! block name " + bname + " setIn file " + filename + " does not match the block name specified setIn bbx file " + fname + " (" + bbx.blockName() + ").");
                        }
                        nl.addBlock(bbx);
                        break;
                    case "BLOCK":
                        //this line won't work
                        String btx = splitLine[2];
                        switch(btx) {
                            case "BASIC":
                                //all basicBoxes are handled here.
                                basicBoxes.Box bx = basicBoxes.Box.valueOf(splitLine[3]);
                                Block.blockType bt = Block.blockType.valueOf(splitLine[2]);
                                Block basic;
                                if(basicBoxes.isStanardGate(bx)){
                                    basic = new Block(bt,bx,splitLine[1], splitLine[4].length() - splitLine[4].replace("=","").length() - 1);
                                    nl.addBlock(basic);
                                }
                                else {
                                    basic = new Block(bt,bx,splitLine[1],false);
                                    nl.addBlock(basic);
                                }
                                break;
                            default:
                                //anything else that implements the NativeBlock interface is handled here.
                                String[] blocknets = splitLine[4].split(",");
                                for(int i = 0; i < blocknets.length; i++) {
                                    blocknets[i] = blocknets[i].split("=")[0];
                                }
                                //blocknets now contains the name of every net. now, we just have to split them into their respective groups.
                                List<String> inputNets = new ArrayList<>();
                                List<String> outputNets = new ArrayList<>();
                                List<String> inoutNets = new ArrayList<>();
                                for(int i = 0; i < blocknets.length; i++) {
                                    String type = blocknets[i].split("_")[1];
                                    if(type.equals("in")) {
                                        inputNets.add(blocknets[i]);
                                    }
                                    else if (type.equals("out")) {
                                        outputNets.add(blocknets[i]);
                                    }
                                    else if (type.equals("inout")) {
                                        inoutNets.add(blocknets[i]);
                                    }
                                }
                                //now that it's all split out, we can generate the NativeBlock and add it to the netlist
                                try {
                                    nl.addBlock(getNative(splitLine[2], splitLine[1], inputNets, outputNets, inoutNets));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;
                        }
                        break;
                    case "NETVALS":
                        nl.addNet(retrieveNetVals(splitLine[1]));
                        break;
                    case "NETPROPERTIES":
                        if(nl.addNetType(retrieveNetTypes(splitLine[1]))) {
                            System.out.println("WARNING! this file has changed the default netTypes for some nets.");
                        }
                        break;
                    case "WIRE":
                        String[] div = splitLine[2].replaceAll("\\[","").replaceAll("\\]","").split(",");
                        nl.addWire(splitLine[1],div);
                        break;

                }
            }

        } catch(IOException e) {
            e.printStackTrace();
            return null;
        }
        return nl;
    }


    private static HashMap<String,Boolean> retrieveNetVals(String h) {
        HashMap<String,Boolean> out = new HashMap<>();
        String[] eachNet = h.split(",");
        for(String n : eachNet) {
            out.put(n.split("=")[0],Boolean.valueOf(n.split("=")[1]));
        }
        return out;
    }
    private static HashMap<String,NetList.netType> retrieveNetTypes(String h) {
        HashMap<String,NetList.netType> out = new HashMap<>();
        String[] eachNet = h.split(",");
        for(String n : eachNet) {
            out.put(n.split("=")[0],NetList.netType.valueOf(n.split("=")[1]));
        }
        return out;
    }
    private static NativeBlock getNative(String type, String name, List<String> ins, List<String> outs, List<String> inouts) throws Exception {
        //this returns the nativeblock specified by the contents of the file.
        switch(type) {
            case "7SegmentDisplayDriver":
                return new SevenSegment(name,ins,outs);
            case "Multiplexer":
                return new Multiplexer(name,ins,outs);
        }
        throw new Exception("NetListFile.getNative: could not find proper NativeBlock for type (" + type + ") and name (" + name + ")!");
    }
}
