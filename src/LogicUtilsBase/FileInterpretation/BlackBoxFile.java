package LogicUtilsBase.FileInterpretation;

import LogicUtilsBase.LogicBlocks.Block;
import LogicUtilsBase.LogicBlocks.NetList;
import LogicUtilsBase.LogicBlocks.blackBox;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by evan on 4/16/2017. This class saves the properties of blackboxes to text files. It can also be passed
 * a netList, and it will produce savefiles for all the given blackboxes, then return the mapping of the blackbox
 * names to their filenames.
 */
public class BlackBoxFile {
    private HashMap<String,String> fileNames = new HashMap<>();
    public BlackBoxFile(){

    }

    final HashMap<String,String> write(NetList netList) {
        for(String b : netList.allBlocks().keySet()) {
            if(netList.allBlocks().get(b).getBlockType() == Block.blockType.BLACKBOX) {
                BlackBoxFile sub = new BlackBoxFile();
                sub.write(((Block)netList.allBlocks().get(b)).getBlkBoxObject(),b.concat(".bbx"), netList.allBlocks().get(b).blockName());
                fileNames.put(b,b.concat(".bbx"));
            }
        }
        return fileNames;
    }

    public static void write(blackBox bbx, String filename, String nameOfBlock) {
        if(!filename.contains(".bbx")) filename = filename.concat(".bbx");
        FileWriter fw = null;
        BufferedWriter bw = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("BLOCKNAME:").append(nameOfBlock).append("\n")
                    .append("INPUT:").append(bbx.getInputNames()).append("\n")
                    .append("OUTPUT:").append(bbx.getOutputNames()).append("\n")
                    .append("STATEMENTS:").append(bbx.getStatements());
            fw = new FileWriter(filename);
            bw = new BufferedWriter(fw);
            bw.write(sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null)
                    bw.close();
                if (fw != null)
                    fw.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static Block openBBX(String filename) {
        if(!filename.contains(".bbx")) filename = filename.concat(".bbx");
        HashMap<String,Character> inputs = null;
        HashMap<String,Character> outputs = null;
        HashMap<Character,String> statements = null;
        String blockName = null;
        try(BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            while((line = br.readLine()) != null) {
                String[] splitLine = line.split(":");
                switch(splitLine[0]) {
                    case "BLOCKNAME":
                        blockName = splitLine[1];
                        break;
                    case "INPUT":
                        inputs = new HashMap<>(processStringChar(splitLine[1],blockName));
                        break;
                    case "OUTPUT":
                        outputs = new HashMap<>(processStringChar(splitLine[1],blockName));
                        break;
                    case "STATEMENTS":
                        statements = new HashMap<>(processCharString(splitLine[1]));
                        break;
                }
            }

        } catch(IOException e) {
            e.printStackTrace();
            return null;
        }
        return new Block(Block.blockType.BLACKBOX,blockName,inputs,outputs,statements);
    }

    private static HashMap<String,Character> processStringChar(String sub,String blockname) {
        HashMap<String,Character> outMap = new HashMap<>();

        sub = sub.replaceAll("[ {}]",""); //remove all spaces and curly brackets

        String[] element = sub.split(",");
        for(String e : element) {
            outMap.put(e.split("=")[0],e.split("=")[1].charAt(0));
        }
        return outMap;
    }
    private static HashMap<Character,String> processCharString(String sub) {
        HashMap<Character,String> outMap = new HashMap<>();

        sub = sub.replaceAll("[ {}]",""); //remove all spaces and curly brackets

        String[] element = sub.split(",");
        for(String e : element) {
            outMap.put(e.split("=")[0].charAt(0),e.split("=")[1]);
        }
        return outMap;
    }
}
