package LogicUtilsBase.FileInterpretation;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import org.w3c.dom.*;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.net.*;
import java.util.HashMap;

import static LogicUtilsBase.FileInterpretation.SettingsFile.Configs.*;
import static LogicUtilsBase.FileInterpretation.SettingsFile.Settings.*;


/**
 * Created by evan on 4/25/2017. This class manages the creation and modification of settings files, as well as all
 * all references to the savings file used throughout LogicTools. The settings file uses the XML format.
 */
public class SettingsFile {
    public enum Settings{
        THEME
    }
    public enum Configs {
        LIGHT,
        DARK,
        NULL
    }

    HashMap<Settings,Configs> settings = new HashMap<>(); //key = setting name, val = setting

    //upon the instantiation of the SettingsFile object, open the settings file and get all the included properties.
    public SettingsFile() {
        try {
            File file = new File("settings.xml");
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(file);

            document.getDocumentElement().normalize();

            settings.put(THEME,Configs.valueOf(document.getElementsByTagName("THEME").item(0).getTextContent()));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error reading config file. Creating default file.");
            createDefault();
        } finally {
            File file = new File("settings.xml");
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
                    .newInstance();
            try {
                DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
                Document document = documentBuilder.parse(file);
                document.getDocumentElement().normalize();

                settings.put(THEME,Configs.valueOf(document.getElementsByTagName("THEME").item(0).getTextContent()));
            } catch (Exception e) {
                System.out.println("fatal error occurred when loading configuration file.");
            }
        }


    }

    void createDefault() {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            //create root elements here
            Document def = docBuilder.newDocument();
            Element root = def.createElement("settings");
            def.appendChild(root);

            //level 1 elements
            Element theme = def.createElement(THEME.toString());
            theme.appendChild(def.createTextNode(LIGHT.toString()));
            root.appendChild(theme);

            //more levels and attributes


            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer tr = tf.newTransformer();
            DOMSource source = new DOMSource(def);
            StreamResult result = new StreamResult(new File("settings.xml"));

            tr.transform(source, result);

        } catch (Exception p) {
            p.printStackTrace();
        }
    }

    public Configs getSetting(Settings setting) {
        return settings.get(setting);
    }

    public void editConfig(Settings setting, Configs config) {
        try {
            File file = new File("settings.xml");
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(file);

            document.getDocumentElement().normalize();

            //settings.put(THEME,getConf(document.getElementsByTagName("THEME").item(0).getTextContent()));

            settings.put(setting,config);

            NodeList mainNode = document.getChildNodes();
            NodeList childNodes = mainNode.item(0).getChildNodes();

            for(int i = 0; i < childNodes.getLength(); i++ ) {
                System.out.println(childNodes.item(i).getNodeName());
                if(setting.toString().equals(childNodes.item(i).getNodeName())) {
                    System.out.println("writing " + setting.toString() + " to " + childNodes.item(i).getNodeName());
                    childNodes.item(i).setTextContent(config.toString());
                }
            }
            
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new File("settings.xml"));
            transformer.transform(source, result);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    static URL createURL(String fileName)
    {
        URL url = null;
        try
        {
            url = new URL(fileName);
        }
        catch (MalformedURLException ex)
        {
            File f = new File(fileName);
            try
            {
                String path = f.getAbsolutePath();
                String fs = System.getProperty("file.separator");
                if (fs.length() == 1)
                {
                    char sep = fs.charAt(0);
                    if (sep != '/')
                        path = path.replace(sep, '/');
                    if (path.charAt(0) != '/')
                        path = '/' + path;
                }
                path = "file://" + path;
                url = new URL(path);
            }
            catch (MalformedURLException e)
            {
                System.out.println("Cannot create url for: " + fileName);
                System.exit(0);
            }
        }
        return url;
    }
}
