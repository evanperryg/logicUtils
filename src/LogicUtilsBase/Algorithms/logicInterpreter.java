package LogicUtilsBase.Algorithms;

import java.util.ArrayList;
import java.util.List;

import static java.lang.StrictMath.pow;

/**
 * Created by evan on 3/29/2017.
 * For the most part, this is a broader implementation of simpleParser, allowing one object to contain multiple outputs
 * that relate to the same input terms. This is also where truth tables for individual boolean functions are made.
 * getErrors() gets passed to the JavaFX textArea being used as a console, allowing for easier troubleshooting.
 */
public class logicInterpreter {
    private String errorMsg = "";
    private final char[] input = new char[20];
    private final char[] output = new char[20];
    private int inC = 0, outC = 0;
    private boolean debugMode;
    private final String[] newStatements = new String[20];
    private final List<String> minterms = new ArrayList<>();

    public logicInterpreter() {
    }
    public logicInterpreter(boolean showDebugPrompts) {
        debugMode = showDebugPrompts;
    }
    public logicInterpreter(boolean showDebugPrompts,String inputChars,String outputChars,String[] statements) {
        debugMode = showDebugPrompts;
        if(statements.length != outputChars.length()) {
            if(debugMode) System.out.println("logicInterpreter(inputChars,outputChars,statements) number of statements is not equal to number of output chars.");
            errorMsg += "logicInterpreter: number of statements unequal to number of designated outputs.\n";
        }
        else {
            for (int i = 0; i < inputChars.length(); i++) addInput(inputChars.charAt(i));
            for (int i = 0; i < outputChars.length(); i++) addOutput(outputChars.charAt(i));
            for (int i = 0; i < statements.length; i++) addStatement(output[i], statements[i]);
        }
    }
    public logicInterpreter(boolean showDebugPrompts,char[] inputChars,char[] outputChars,String[] statements) {
        debugMode = showDebugPrompts;
        if(statements.length != outputChars.length) {
            if(debugMode) System.out.println("logicInterpreter(inputChars,outputChars,statements) number of statements is not equal to number of output chars.");
            errorMsg += "logicInterpreter: number of statements unequal to number of designated outputs.\n";
        }
        else {
            for (char inputChar : inputChars) addInput(inputChar);
            for (char outputChar : outputChars) addOutput(outputChar);
            for (int i = 0; i < statements.length; i++) addStatement(output[i], statements[i]);
        }
    }

    public int inputCount() {return inC;}
    public int outputCount() {return outC;}
    public char inputAt(int i) {return input[i];}
    public char outputAt(int i) {return output[i];}

    private boolean verifyInputNames(char name) {
        char[] notAllowed = {'0','1','*','%','/','+','(',')','^',',',' '};
        for (char aNotAllowed : notAllowed) {
            if (name == aNotAllowed) return false;
        }
        return true;
    }
    private void addInput(char name) {
        if(verifyInputNames(name)) {
            if (debugMode) System.out.println("logicInterpreter.addInput(" + name + ") inC=" + inC);
            input[inC] = name;
            if (debugMode) System.out.println("logicInterpreter.addInput(" + name + ") input[inC]=" + input[inC]);
            inC++;
        }
        else {
            if (debugMode) System.out.println("logicInterpreter.addInput(" + name + ") was not added. Invalid name.");
        }
    }
    private void addOutput(char name) {
        if(verifyInputNames(name)) {
            if (debugMode) System.out.println("logicInterpreter.addOutput(" + name + ") outC=" + outC);
            output[outC] = name;
            if (debugMode) System.out.println("logicInterpreter.addOutput(" + name + ") output[outC]=" + output[outC]);
            outC++;
        }
        else {
            if (debugMode) System.out.println("logicInterpreter.addOutput(" + name + ") was not added. Invalid name.");
        }
    }
    private void addStatement(char whichOutput, String statement) {
        for(int i = 0; i < outC; i++) {
            if(output[i] == whichOutput) {
                newStatements[i] = statement;
            }
        }
    }

    private String intToBin(int toBin, int place) {
        if (toBin == 0 && place == 0) return "";
        if (toBin >= pow(2, (place - 1))) {
            toBin -= pow(2, (place - 1));
            return "1" + intToBin(toBin, place - 1);
        } else {
            return "0" + intToBin(toBin, place - 1);
        }
    }

    public boolean solve(char whichOutput, String binaryInputs) {
        int p = -1;
        for (int i = 0; i < outC; i++) {
            if(output[i] == whichOutput) p = i;
        }
        if(debugMode) System.out.println("logicInterpreter.solve("+whichOutput+","+binaryInputs+") p="+p);
        if(p == -1) return false;

        String eqn = newStatements[p];
        simpleParser a = new simpleParser(eqn, binaryInputs, input, debugMode);
        String n = a.process();
        //System.readOut.println(n);
        return Integer.valueOf(n) != 0;
    }
    public int solveInt(char whichOutput, String binaryInputs) {
        int p = -1;
        for (int i = 0; i < outC; i++) {
            if(output[i] == whichOutput) p = i;
        }
        if(debugMode) System.out.println("logicInterpreter.solve("+whichOutput+","+binaryInputs+") p="+p);
        if(p == -1) return 0;

        String eqn = newStatements[p];
        simpleParser a = new simpleParser(eqn, binaryInputs, input, debugMode);
        String n = a.process();
        //System.readOut.println(n);
        if(Integer.valueOf(n) != 0) return 1;
        else return 0;
    }
    public String truthTable(char whichOutput) {
        int p = -1;
        for (int i = 0; i < outC; i++) {
            if(output[i] == whichOutput) p = i;
        }
        if(debugMode) System.out.println("logicInterpreter.truthTable("+whichOutput+") p="+p);
        if(p == -1) return "logicInterpreter.truthTable("+whichOutput+"): requested output not found";

        String eqn = newStatements[p];
        simpleParser a = new simpleParser(eqn, inC, input, debugMode);
        StringBuilder n = new StringBuilder();
        for (int cycle = 0; cycle < pow(2,inC); cycle++) {
            String m = intToBin(cycle, inC);
            String q = a.process(m);
            //if(debugMode) System.readOut.println("logicInterpreter.truthTable("+whichOutput+") m="+m);
            if (!q.equalsIgnoreCase("0")) minterms.add(m);
            n.append(m).append("  ").append(q).append("\n");
        }
        errorMsg += a.getErrors();
        //System.readOut.println(n);
        return n.toString();
    }
    public String truthTableWithHeader(char whichOutput) {
        int p = -1;
        for (int i = 0; i < outC; i++) {
            if(output[i] == whichOutput) p = i;
        }
        if(debugMode) System.out.println("logicInterpreter.truthTable("+whichOutput+") p="+p);
        if(p == -1) return "logicInterpreter.truthTable("+whichOutput+"): requested output not found";

        String eqn = newStatements[p];
        simpleParser a = new simpleParser(eqn, inC, input, debugMode);
        StringBuilder n = new StringBuilder();
        for (char ch : input) {
            n.append(ch);
        }
        n.append("  ").append(whichOutput).append("\n");
        for (int cycle = 0; cycle < pow(2,inC); cycle++) {
            String m = intToBin(cycle, inC);
            String q = a.process(m);
            //if(debugMode) System.readOut.println("logicInterpreter.truthTable("+whichOutput+") m="+m);
            if (!q.equalsIgnoreCase("0")) minterms.add(m);
            n.append(m).append("  ").append(q).append("\n");
        }
        errorMsg += a.getErrors();
        //System.readOut.println(n);
        return n.toString();
    }
    public String simulate(char whichOutput, int[] binSeries) {
        int p = -1;
        for (int i = 0; i < outC; i++) {
            if(output[i] == whichOutput) p = i;
        }
        if(debugMode) System.out.println("logicInterpreter.simulate("+whichOutput+") p="+p);
        if(p == -1) return "logicInterpreter.simulate("+whichOutput+"): requested output not found";

        String eqn = newStatements[p];
        simpleParser a = new simpleParser(eqn, inC, input, debugMode);
        StringBuilder n = new StringBuilder();
        for (int binSery : binSeries) {
            String m = intToBin(binSery, inC);
            n.append(a.process(m));
        }
        return n.toString();
    }
    public List<String> getMinterms(){
        List<String> a = new ArrayList<>();
        a.clear();
        a.addAll(minterms);
        return a;
    }

    public String getErrors() {
        return errorMsg;
    }
}
