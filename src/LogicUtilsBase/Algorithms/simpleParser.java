package LogicUtilsBase.Algorithms;


import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * Created by evan on 3/29/2017.
 * Parse and solve boolean equations entered setIn a natural-language style, for example, "a'b+b'c"
 * it is worth noting that the xor symbol (^) DOES work. one simpleParser object takes a single function, the letters
 * corresponding to the inputs, and the values for each of those inputs.
 */
public class simpleParser {
    private String errorMsg = "";
    private String inString;
    private String binIns;
    private String preProcessedOutput;          //the last preprocessed output
    private final char[] terms = new char[20];        //contains the given value for each term
    private final char[] termNames = new char[20];    //contains the corresponding names for each term
    private boolean debugMode;

    //return where the delimiter is
    public simpleParser(String input, String binaryInputs, char inNames[], boolean debug) {
        debugMode = debug;
        inString = input;
        binIns = binaryInputs;
        if(debugMode) System.out.println("simpleParser.simpleParser for input " + inString + "; binaryInputs= "+ binaryInputs);
        for(int i = 0; i < binaryInputs.length(); i++) {
            terms[i] = binaryInputs.charAt(i);
            termNames[i] = inNames[i];
        }
        //preprocess string
        preProcessedOutput = preProcessString(inString);
        if(debugMode) System.out.println("simpleParser.simpleParser for input " + inString + "; preProcessedOutput= " + preProcessedOutput);
    }
    public simpleParser(String input, int inputLength, char inNames[], boolean debug) {
        debugMode = debug;
        inString = input;
        if(debugMode) System.out.println("simpleParser.simpleParser for input " + inString + "; inputLength= "+ inputLength);
        System.arraycopy(inNames, 0, termNames, 0, inputLength);
        //preprocess string
        preProcessedOutput = preProcessString(inString);
        if(debugMode) System.out.println("simpleParser.simpleParser for input " + inString + "; preProcessedOutput= " + preProcessedOutput);
    }
    public simpleParser() {
    }

    String process() {
        return parSolve(inString);
    }
    String process(String binaryInputs) {
        binIns = binaryInputs;
        for(int i = 0; i < binaryInputs.length(); i++) {
            terms[i] = binaryInputs.charAt(i);
        }
        return parSolve(inString);
    }

    //replace the term names with their values, then return the math as a string
    private String parSolve(String initEq) {
        String preEq = preProcessedOutput;
        //replace terms with their values

        for (int i = 0; i < binIns.length(); i++) {
            //System.readOut.println("preEq= " + preEq);
            try {
                preEq = preEq.replace(termNames[i], terms[i]);
            } catch (NullPointerException e) {
                if (debugMode)
                    System.out.println("simpleParser.parSolve for input " + inString + "; NullPointerException caught. Unequal size for termNames[] and terms[]?");
                errorMsg = errorMsg.concat("internal error while parsing input function. Unequal count of input terms and termNames.\n");
            }
        }
        if (debugMode)
            System.out.println("simpleParser.parSolve for input " + inString + "; values entered preEq= " + preEq);
        //add inverter code here
        String subEq = postProcessString(preEq);
        if (debugMode)
            System.out.println("simpleParser.parSolve for input " + inString + "; postprocessed subEq= " + subEq);

        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName("JavaScript");
        try {
            //System.readOut.println(String.valueOf(engine.eval(subEq)));
            String s = String.valueOf(engine.eval(subEq));
            if (!s.equals("0")) return "1";
            else return "0";
        } catch (ScriptException e) {
            errorMsg += "Solving of the input function failed. This is usually due to imbalanced parenthesis.\n";
            e.printStackTrace();
            return "";
        }

    }
    //do things that can't be done after the variables have been replaced with their values
    //chiefly, adding setIn * where needed.
    private String preProcessString(String subEq) {
        String returnString = "";
        returnString += subEq.charAt(0);
        StringBuilder returnStringBuilder = new StringBuilder(returnString);
        for(int i = 1; i < subEq.length(); i++) {
            if( (isVar(subEq.charAt(i)) && isVar(subEq.charAt(i-1))) ||
                (subEq.charAt(i-1) == '\'' && isVar(subEq.charAt(i))) ||
                (isVar(subEq.charAt(i-1)) && subEq.charAt(i) == '(') ||
                (isVar(subEq.charAt(i)) && subEq.charAt(i-1) == ')') ||
                (subEq.charAt(i-1) == '\'' && subEq.charAt(i) == '(') ||
                (Character.isDigit(subEq.charAt(i-1)) && (subEq.charAt(i) != '+') && (subEq.charAt(i) != '\''))) {
                returnStringBuilder.append("*");
            }
            returnStringBuilder.append(subEq.charAt(i));
        }
        returnString = returnStringBuilder.toString();
        preProcessedOutput = returnString;
        return returnString;
    }
    //fix the little details, like apostrophes
    private String postProcessString(String subEq) {
        String outString = "";
        for(int i = 0; i < subEq.length(); i++) {
            //convert the apostrophes to inverts
            if (i < subEq.length() - 1 && (subEq.charAt(i + 1) == '\'')) {
                if (subEq.charAt(i) == '0') outString += "1";
                else if (subEq.charAt(i) == '1') outString += "0";
                else if (subEq.charAt(i) == ')') {
                    outString+=")";
                    if (debugMode)
                        System.out.println("simpleParser.parSolve.postProcessString for input " + inString + "; inverted setIn-parenthesis detected. outString is " + outString);
                    if (debugMode)
                        System.out.println("simpleParser.parSolve.postProcessString for input " + inString + "; matching open parenthesis at " + findMatchingOpenParenthesis(outString));
                    //this is wrong. need to find the matching set of parenthesis, not the first two that show up:
                    //sub = sub.substring(sub.indexOf("(")+1,sub.indexOf(")"));
                    //however, this IS right. use the findMatchingOpenParenthesis() function to get the actual beginning of the inverted statement we want to evaluate:
                    String sub = outString.substring(findMatchingOpenParenthesis(outString));
                    if (debugMode)
                        System.out.println("simpleParser.parSolve.postProcessString for input " + inString + "; inverted setIn-parenthesis phrase recognized as " + sub);

                    //use jscript engine to solve
                    ScriptEngineManager mgr = new ScriptEngineManager();
                    ScriptEngine engine = mgr.getEngineByName("JavaScript");
                    String s = null;
                    try {
                        s = String.valueOf(engine.eval(sub));
                    } catch (ScriptException e) {
                        e.printStackTrace();
                        errorMsg = errorMsg.concat("Error setIn evaluating the post-processed string. This is generally caused by incorrectly-placed apostrophes or parenthesis.\n");
                    }
                    if(!s.equals("0")) outString = outString.substring(0,findMatchingOpenParenthesis(outString)) + "0";
                    else outString = outString.substring(0,findMatchingOpenParenthesis(outString)) + "1";
                    if (debugMode) {
                        System.out.println("simpleParser.parSolve.postProcessString for input " + inString + "; post-modification outString is " + outString);
                    }
                }
                i++;
            }
            //other stuff
            else
            {
                outString += String.valueOf(subEq.charAt(i));
            }
        }
        //System.readOut.println("outString=" + outString);
        return outString;
    }

    private int findMatchingOpenParenthesis(String inStr) {
        //since outString will always end with the close parenthesis matching
        //the open parenthesis we want, we can index backwards. count the number
        //of open and close parenthesis. Once we reach the corresponding open
        //parenthesis for the one at the end of the string, return its position.
        int closesFound = 0, opensFound = 0;
        for(int i = inStr.length() - 1; i >= 0; i--) {
            //since, setIn this application, the string is always going to start
            //with a close parenthesis, this is fine.
            if(inStr.charAt(i) == ')') {
                closesFound++;
            }
            else if(inStr.charAt(i) == '(') {
                opensFound++;
            }
            if(closesFound == opensFound) return i;
        }
        return 0;
    }

    private boolean isVar(char c) {
        for(int i = 0; i < 20; i++) {
            if(c == termNames[i]) return true;
        }
        return false;
    }

    public String getErrors(){
        return errorMsg;
    }
}
