package LogicUtilsBase.Algorithms;

import java.util.*;

/**
 * Created by evan on 4/3/2017.
 * An implementation of the Quine-McCluskey algorithm. Takes an array (or list) of strings that represent each
 * term- for example, {"1011","1XX0","0001"}, and an array of chars representing the character names of each
 * variable setIn each given term. getErrors() is passed up to the JavaFX textArea being used as a console, allowing the
 * console to show valuable error messages.
 */
public class QuineMcCluskey {
    private int differenceLoc;
    private String a,b;
    private final String[] minTermArr;
    private final List<String> minTerms = new ArrayList<>();
    private final char[] inputsList;
    private final boolean debugMode;

    public QuineMcCluskey(String[] termArray,char[] inputNames,boolean showDebugPrompts){
        minTermArr = termArray;
        inputsList = inputNames;
        debugMode = showDebugPrompts;
        Collections.addAll(minTerms, minTermArr);
    }
    public QuineMcCluskey(String[] termArray,char[] inputNames){
        minTermArr = termArray;
        inputsList = inputNames;
        debugMode = false;
        Collections.addAll(minTerms, minTermArr);
    }
    public QuineMcCluskey(List<String> minTermList,char[] inputNames){
        minTermArr = new String[minTermList.size()];
        int[] i = new int[1];
        i[0] = 0;
        minTermList.forEach((element) -> {
            minTermArr[i[0]] = element;
            i[0]++;
        });
        inputsList = inputNames;
        debugMode = false;
    }
    public QuineMcCluskey(List<String> minTermList,char[] inputNames,boolean showDebugPrompts){
        minTermArr = new String[minTermList.size()];
        int[] i = new int[1];
        i[0] = 0;
        minTermList.forEach((element) -> {
            minTermArr[i[0]] = element;
            i[0]++;
        });
        inputsList = inputNames;
        System.out.println(inputsList.length);
        debugMode = showDebugPrompts;
    }

    private void update(String stringA, String stringB){
        a = stringA;
        b = stringB;
    }

    private String combine(){
        StringBuilder out = new StringBuilder();
        if(a.length() != b.length()) return "";
        for(int i = 0; i < a.length(); i++) {
            if(i == differenceLoc) out.append("X");
            else out.append(a.charAt(i));
        }
        return out.toString();
    }
    private boolean oneDifferenceBetween(){
        int dl = -1;
        if(a.length() != b.length()) return false;
        int cnt = 0;
        for(int i = 0; i < a.length(); i++) {
            if(a.charAt(i) != b.charAt(i)) {
                cnt++;
                dl = i;
            }
        }
        if(cnt == 1) {
            differenceLoc = dl;
            return true;
        }
        else return false;
    }
    public void simplify(){
        run(minTermArr);
    }
    private void run(String[] strArr) {
        minTerms.clear();
        minTerms.addAll(Arrays.asList(strArr));
        if(debugMode) {
            for (int i = 0; i < minTerms.size(); i++) {
                System.out.println("QuineMcCluskey.run: initial list minTerms[" + i + "]= " + String.valueOf(minTerms.get(i)));
            }
        }

        //use a hash set to store all unique simplifications.
        //Current big O is (n^n), where n is the
        //number of terms to be simplified. Clearly this isn't
        //acceptable, but
        int cycle = 0, lastTermsCount, newTermsCount = 0;
        Set<String> newTerms = new HashSet<>();
        do{
            lastTermsCount = newTermsCount;
            if(debugMode) System.out.println("QuineMcCluskey.run: BEGIN CYCLE " + cycle);
            boolean notSimplified;
            newTerms.clear();
            //simplify
            for (int i = 0; i < minTerms.size(); i++) {
                notSimplified = true;
                for (int j = 0; j < minTerms.size(); j++) {
                    update(String.valueOf(minTerms.get(i)), String.valueOf(minTerms.get(j)));
                    if (oneDifferenceBetween()) {
                        //found a combo
                        if(debugMode) System.out.println("QuineMcCluskey.run: one difference between elements " + minTerms.get(i) + "("+ i + ") " + minTerms.get(j) + "("+ j + ") becomes " + String.valueOf(combine()));
                        newTerms.add(String.valueOf(combine()));
                        notSimplified = false;
                    }
                }
                if(notSimplified){
                    newTerms.add(String.valueOf(minTerms.get(i)));
                    if(debugMode) System.out.println("QuineMcCluskey.run: no suitable simplifications for " + String.valueOf(minTerms.get(i)));
                }
            }
            if(debugMode) {
                System.out.print("QuineMcCluskey.run: newTerms= ");
                newTerms.forEach((element) -> System.out.print(element + " "));
                System.out.println();
            }
            newTermsCount = newTerms.size();
            //move new simplified set into the output array, rinse and repeat
            minTerms.clear();
            minTerms.addAll(newTerms);
            newTerms.clear();
            cycle++;
        }while(lastTermsCount != newTermsCount);
    }

    public String convertToExpression() {
        final String[] out = {""};
        if(debugMode) System.out.println("QuineMcCluskey.convertToExpression: size of list= "+ minTerms.size());
        final int[] allX = {0};
        minTerms.forEach((element) -> {
            for(int i = 0; i < inputsList.length; i++) {
                switch(element.charAt(i)) {
                    case '0':
                        out[0] += (String.valueOf(inputsList[i] + "'"));
                        break;
                    case '1':
                        out[0] += String.valueOf(inputsList[i]);
                        break;
                    case 'X':
                        allX[0]++;
                }
            }
            if(allX[0] == inputsList.length) out[0] = "1";
            if(debugMode) System.out.println("QuineMcCluskey.convertToExpression: for element(" + element + ") readOut= " + out[0]);
            out[0] +="+";
            allX[0] = 0;
        });
        if(out[0].isEmpty()) return "0";
        else return out[0].substring(0, out[0].length()-1);
    }

    public String getErrors(){
        return "";
    }
}
