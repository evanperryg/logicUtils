package LogicUtilsBase;

import LogicUtilsBase.Algorithms.QuineMcCluskey;
import LogicUtilsBase.Algorithms.logicInterpreter;
import LogicUtilsBase.FileInterpretation.NetListFile;
import LogicUtilsBase.FileInterpretation.SettingsFile;
import LogicUtilsBase.LogicBlocks.*;

import LogicUtilsBase.LogicBlocks.MSI.Multiplexer;
import LogicUtilsBase.LogicBlocks.MSI.ValuedBlock;
import LogicUtilsBase.LogicBlocks.MSI.SevenSegment;
import LogicUtilsBase.etc.*;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.*;
import javafx.stage.*;

import java.net.URL;
import java.util.ResourceBundle;

import java.util.*;

import static LogicUtilsBase.LogicBlocks.Block.blockType.*;
import static LogicUtilsBase.LogicBlocks.NetList.netType.*;
import static LogicUtilsBase.LogicBlocks.basicBoxes.Box.*;
import static LogicUtilsBase.LogicToolsMain.PassIn.*;
import static LogicUtilsBase.MainExtras.*;

/**
 * Created by evan on 4/11/2017.
 * The handling class for the main window.
 */
public class LogicToolsMain extends Application implements Initializable {
    //TODO: add icons to all the windows.

    public Button seqSimButton;
    public Button booleanSimpButton;
    public TextArea consoleArea;
    public AnchorPane anchorPane;
    public MenuItem fileClose;
    public ProgressBar progressBar;
    public TextField consoleField;

    public AnchorPane accordionAnchor4;
    public AnchorPane accordionAnchor1;
    public AnchorPane accordionAnchor3;
    public Accordion fullAccordion;
    public TitledPane x3;
    public TitledPane x2;
    public TitledPane x1;
    @FXML private MenuItem simTreeContextMenuProperties;
    @FXML private ContextMenu simTreeContextMenu;
    @FXML private TreeView simTreeView;
    @FXML private RadioMenuItem darkThemeRB;
    @FXML private MenuItem wireTreeContextMenuRemoveNetList;
    @FXML private MenuItem wireTreeContextMenuAddWire;
    @FXML private SeparatorMenuItem wireTreeContextMenuDivider1;
    @FXML private MenuItem wireTreeContextMenuRemoveWire;
    @FXML private MenuItem wireTreeContextMenuRemoveWireNet;
    @FXML private MenuItem wireTreeContextMenuProperties;
    @FXML private ContextMenu wireTreeContextMenu;
    @FXML private MenuItem blockTreeContextMenuAddWire;
    @FXML private MenuItem blockTreeContextMenuRemoveNetList;
    @FXML private SeparatorMenuItem blockTreeContextMenuDivider1;
    @FXML private MenuItem blockTreeContextMenuAddBlock;
    @FXML private MenuItem blockTreeContextMenuProperties;
    @FXML private MenuItem blockTreeContextMenuRemoveBlock;
    @FXML private ContextMenu blockTreeContextMenu;
    @FXML private TreeView blocksTreeView;
    @FXML private TreeView wiresTreeView;

    enum PassIn {
        CONSOLE,
        GUI,
        NULL
    }
    private PassIn thisPass = NULL;

    private String selectedNet = null;
    private String selectedBlock = null;
    private String selectedNetList = null;
    private String selectedWire = null;
    private String selectedSimulation = null;

    private String consoleActiveNetList = null;

    private TreeItem<String> blockTreeRoot;
    private TreeItem<String> wireTreeRoot;
    private TreeItem<String> simTreeRoot;

    public ArrayList<NetList> netLists = new ArrayList<>();
    public ArrayList<CombinationalSimulation> cSims = new ArrayList<>();

    private HistoryTracker consoleHistory = new HistoryTracker(20);
    private int histToGet = -1;
    //KeyCode lastKeyInConsole;

    private SettingsFile sf = new SettingsFile();

    private FXConsole console = new FXConsole();
    private Diary diary = new Diary();

    private Scene mainscene;
    private Stage mainstage;

    public static void main(String[] args) {
        //System.readOut.println(sf.getSetting(SettingsFile.Settings.THEME));
        launch(args);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //decide whether the checkbox for darkTheme should be selected
        if(sf.getSetting(SettingsFile.Settings.THEME) == SettingsFile.Configs.DARK) {
            darkThemeRB.setSelected(true);
        }

        //set up tree views
        blockTreeRoot = new TreeItem<>("Root");
        wireTreeRoot = new TreeItem<>("Root");
        simTreeRoot = new TreeItem<>("Root");

        blocksTreeView.setRoot(blockTreeRoot);
        wiresTreeView.setRoot(wireTreeRoot);
        simTreeView.setRoot(simTreeRoot);
        blocksTreeView.setShowRoot(false);
        wiresTreeView.setShowRoot(false);
        simTreeView.setShowRoot(false);

        blocksTreeView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                TreeItem<String> selectedItem = (TreeItem<String>) newValue;
                TreeItem<String> parent1 = ((TreeItem<String>) newValue).getParent();
                TreeItem<String> parent2 = null;
                try {
                    parent2 = ((TreeItem<String>) parent1).getParent();
                    //if the level 2 parent is the root, then we have selected a block
                    //TODO: if the user names a netlist "Root," then the program will think a selected net is a selected block. This needs fixing.
                    if(parent2.getValue().equals("Root")) {
                        //a block has been selected
                        selectedNet = null;
                        selectedBlock = selectedItem.getValue();
                        selectedNetList = parent1.getValue();
                        selectedWire = null;
                        selectedSimulation = null;
                        blockTreeContextMenu.getItems().clear();
                        blockTreeContextMenu.getItems().addAll(blockTreeContextMenuProperties,blockTreeContextMenuDivider1,blockTreeContextMenuRemoveBlock);
                    }
                    //if the level 2 parent isn't root
                    else {
                        //a net has been selected
                        selectedNet = selectedItem.getValue();
                        selectedBlock = parent1.getValue();
                        selectedNetList = parent2.getValue();
                        selectedWire = null;
                        selectedSimulation = null;
                        blockTreeContextMenu.getItems().clear();
                        blockTreeContextMenu.getItems().addAll(blockTreeContextMenuProperties);
                    }
                } catch (NullPointerException e) {
                    //this only gets thrown if there is no level 2 parent, aka only if a netlist is selected
                    selectedNet = null;
                    selectedBlock = null;
                    selectedNetList = selectedItem.getValue();
                    selectedWire = null;
                    selectedSimulation = null;
                    blockTreeContextMenu.getItems().clear();
                    blockTreeContextMenu.getItems().addAll(blockTreeContextMenuProperties,blockTreeContextMenuDivider1,blockTreeContextMenuAddWire,blockTreeContextMenuAddBlock,blockTreeContextMenuRemoveNetList);
                }
                System.out.println("Netlist: " + selectedNetList + "   Block: " + selectedBlock + "   Wire: " + selectedWire + "   Net: " + selectedNet + "   Sim: " + selectedSimulation);
            }
        });
        wiresTreeView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                TreeItem<String> selectedItem = (TreeItem<String>) newValue;
                TreeItem<String> parent1 = selectedItem.getParent();
                TreeItem<String> parent2 = null;
                try {
                    parent2 = parent1.getParent();
                    //TODO: if the user names a netlist "Root," then the program will think a selected net is a selected wire. This needs fixing.
                    if(parent2.getValue().equals("Root")) {
                        //a wire has been selected
                        selectedNet = null;
                        selectedBlock = null;
                        selectedNetList = parent1.getValue();
                        selectedWire = selectedItem.getValue();
                        selectedSimulation = null;
                        wireTreeContextMenu.getItems().clear();
                        wireTreeContextMenu.getItems().addAll(wireTreeContextMenuProperties,wireTreeContextMenuDivider1,wireTreeContextMenuAddWire,wireTreeContextMenuRemoveWire);
                    }
                    else {
                        //a net has been selected
                        selectedNet = selectedItem.getValue();
                        selectedBlock = null;
                        selectedNetList = parent2.getValue();
                        selectedWire = parent1.getValue();
                        selectedSimulation = null;
                        wireTreeContextMenu.getItems().clear();
                        wireTreeContextMenu.getItems().addAll(wireTreeContextMenuProperties,wireTreeContextMenuDivider1,wireTreeContextMenuRemoveWireNet);
                    }
                } catch (NullPointerException e) {
                    //if it's a nullptr exception, we selected a netlist.
                    selectedNet = null;
                    selectedBlock = null;
                    selectedNetList = selectedItem.getValue();
                    selectedWire = null;
                    selectedSimulation = null;
                    wireTreeContextMenu.getItems().clear();
                    wireTreeContextMenu.getItems().addAll(wireTreeContextMenuProperties,wireTreeContextMenuDivider1,wireTreeContextMenuAddWire,wireTreeContextMenuRemoveNetList);
                }
                System.out.println("Netlist: " + selectedNetList + "   Block: " + selectedBlock + "   Wire: " + selectedWire + "   Net: " + selectedNet + "   Sim: " + selectedSimulation);
            }
        });
        simTreeView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                TreeItem<String> selectedItem = (TreeItem<String>) newValue;
                TreeItem<String> parent1 = selectedItem.getParent();
                TreeItem<String> parent2 = null;
                try {
                    parent2 = parent1.getParent();
                    if(parent2.getValue().equals("Root")) {
                        //a simulation has been selected
                        selectedNet = null;
                        selectedBlock = null;
                        selectedNetList = parent1.getValue();
                        selectedWire = null;
                        selectedSimulation = selectedItem.getValue();
                        simTreeContextMenu.getItems().clear();
                        simTreeContextMenu.getItems().addAll(simTreeContextMenuProperties);
                    }
                    else {
                        //a wire has been selected
                        selectedNet = null;
                        selectedBlock = null;
                        selectedNetList = parent2.getValue();
                        selectedWire = selectedItem.getValue();
                        selectedSimulation = parent1.getValue();
                        simTreeContextMenu.getItems().clear();
                        simTreeContextMenu.getItems().addAll(simTreeContextMenuProperties);
                    }

                } catch (NullPointerException e) {
                    //a netlist has been selected
                    selectedNet = null;
                    selectedBlock = null;
                    selectedNetList = selectedItem.getValue();
                    selectedWire = null;
                    selectedSimulation = null;
                    simTreeContextMenu.getItems().clear();
                    simTreeContextMenu.getItems().addAll(simTreeContextMenuProperties);
                }
                System.out.println("Netlist: " + selectedNetList + "   Block: " + selectedBlock + "   Wire: " + selectedWire + "   Net: " + selectedNet + "   Sim: " + selectedSimulation);
            }
        });
    }

    @Override
    public void start(Stage mainstage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("ltInterface.fxml"));

        mainscene = new Scene(root, 900, 600);

        //set the theme from the settings file
        if(sf.getSetting(SettingsFile.Settings.THEME) == SettingsFile.Configs.DARK) {
            mainscene.getStylesheets().add
                    (getClass().getResource("utilsTheme.css").toExternalForm());
        }
        else {
            mainscene.getStylesheets().add
                    (getClass().getResource("utilsThemeWhite.css").toExternalForm());
        }

        mainstage.getIcons().add(new Image("/img/and.png"));

        mainstage.setMinHeight(450);
        mainstage.setMinWidth(650);
        mainstage.setResizable(true);     //this is default, but it's here just setIn case
        mainstage.setTitle("LogicTools");

        mainstage.setScene(mainscene);
        mainstage.show();

    }
    

    //TEST METHODS
    void dothings() {
        addNetList(new NetList("netListName"));
        addBlock("netListName", new Block(BASIC,AND,"name_of_block",3));
        addWire("netListName","aWire","name_of_block_in_0");
        addWire("netListName","bWire","name_of_block_in_1");
        addWire("netListName","cWire","name_of_block_in_2");
        addWire("netListName","zWire","name_of_block_out_0");
        for(NetList n : netLists) {
            if(n.name().equals("netListName")) {
                addCSimulation("netListName",new CombinationalSimulation(n,"someSimulation"));
            }
        }
        addCSimNet("someSimulation",SIMULATOR_IN,"aWire");
        addCSimNet("someSimulation",SIMULATOR_IN,"bWire");
        addCSimNet("someSimulation",SIMULATOR_IN,"cWire");
        addCSimNet("someSimulation",SIMULATOR_OUT,"zWire");
        
    }


    //FILE MANAGEMENT METHODS
    @FXML
    public void onSaveAs(ActionEvent actionEvent) {

    }

    @FXML
    public void onSave(ActionEvent actionEvent) {
    }

    @FXML
    public void onSaveAllAs(ActionEvent actionEvent) {
    }

    @FXML
    public void onSaveAll(ActionEvent actionEvent) {
    }


    //EDITING METHODS
    @FXML
    public void onAddBlock(ActionEvent actionEvent) {
        fullAccordion.setExpandedPane(x1);

        Stage newStage = new Stage();
        newStage.initModality(Modality.APPLICATION_MODAL);
        newStage.setTitle("Add Block");
        newStage.initOwner(mainstage);
        newStage.setResizable(false);

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(10, 10, 10, 10));

        List<String> list = new ArrayList<>();
        for(NetList nl : netLists) {
            list.add(nl.name());
        }
        List<String> allblocks = new ArrayList<>();
        allblocks.add("Digital HIGH");
        allblocks.add("Digital LOW");
        allblocks.add("AND gate");
        allblocks.add("OR gate");
        allblocks.add("NAND gate");
        allblocks.add("NOR gate");
        allblocks.add("XOR gate");
        allblocks.add("XNOR gate");
        allblocks.add("Inverter");
        allblocks.add("RS-NOR Latch");
        allblocks.add("RS-NAND Latch");
        allblocks.add("RS-NOR Latch with AND enable");
        allblocks.add("RS-NOR Latch with NAND enable");
        allblocks.add("RS-NAND Latch with AND enable");
        allblocks.add("RS-NAND Latch with NAND enable");
        allblocks.add("D Latch with HIGH enable");
        allblocks.add("JK Latch with HIGH enable");
        allblocks.add("D Flip-Flop with rising edge trigger");
        allblocks.add("D Flip-Flop with falling edge trigger");
        allblocks.add("JK Flip-Flop with rising edge trigger");
        allblocks.add("JK Flip-Flop with falling edge trigger");
        allblocks.add("Hex to 7-segment display decoder");

        //List View of the elements
        ObservableList<String> observableList1 = FXCollections.observableList(allblocks);
        ListView blocklist = new ListView(observableList1);
        grid.add(blocklist,0,0);

        //Text Field of the elements
        TextField searchBox = new TextField();
        searchBox.setPromptText("Search...");
        grid.add(searchBox,0,1);

        //Submit button
        Button addBlock = new Button();
        addBlock.setText("Add Block");
        grid.add(addBlock,1,1);

        //Vbox for properties
        VBox properties = new VBox();
        properties.setSpacing(10);
        properties.setMaxWidth(215);
        properties.setPadding(new Insets(5,5,5,5));

        //Combo View of the netLists
        ObservableList<String> observableList = FXCollections.observableList(list);
        ComboBox netlistlist = new ComboBox(observableList);
        netlistlist.setPrefWidth(195);
        netlistlist.setPromptText("Select NetList...");

        //Textfield for name
        TextField blockName = new TextField();
        blockName.setPromptText("Block name");

        //Textfield for number of inputs
        TextField inCount = new TextField();
        inCount.setPromptText("Input count");

        //Label for name of block
        Text blockTitle = new Text();
        blockTitle.setText("Select a block.");
        blockTitle.setFont(Font.font("Tahoma", FontWeight.BOLD,14));

        //Label for block information
        TextArea blockDescription = new TextArea();
        blockDescription.setEditable(false);
        blockDescription.setFocusTraversable(false);
        blockDescription.setWrapText(true);

        //add it all to the vbox, add the vbox to the grid
        properties.getChildren().addAll(blockTitle,blockDescription,blockName,inCount,netlistlist);
        grid.add(properties,1,0);

        ColumnConstraints column1 = new ColumnConstraints();
        column1.setHalignment(HPos.LEFT);
        grid.getColumnConstraints().add(column1);
        ColumnConstraints column2 = new ColumnConstraints();
        column2.setHalignment(HPos.RIGHT);
        column2.setMaxWidth(215);
        column2.setPrefWidth(215);
        grid.getColumnConstraints().add(column2);

        //if we opened from the console, set the chosen netlist to the one specified setIn the console
        //if we opened from a right click on a netlist, set the chosen netlist to the one we right clicked on
        //if we opened from the menu bar, don't set a chosen netlist
        switch(thisPass) {
            case GUI:
                netlistlist.getSelectionModel().select(selectedNetList);
                break;
            case CONSOLE:
                netlistlist.getSelectionModel().select(consoleActiveNetList);
                break;
        }

        //makes sure only numbers can be added to inCount
        inCount.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*")) {
                    inCount.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        //search box handler
        try {
            searchBox.textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    List<String> matches = new ArrayList<String>();
                    blocklist.getSelectionModel().clearSelection();
                    for(String j : allblocks) {
                        if(j.toLowerCase().replaceAll("[ -]","").contains(newValue.toLowerCase().replaceAll("[ -]",""))) {
                            matches.add(j);
                        }
                    }
                    ObservableList<String> observableListn = FXCollections.observableList(matches);
                        blocklist.setItems(observableListn);

                }
            });
        } catch (NullPointerException e) {
            //this catches a nullpointer exception that occurs when the selected item setIn the list moves
            //during a search. the search and everything else about the program still work fine, so
            //it's fine to just leave this empty for now.
        }
        //when a block is selected
        blocklist.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                //this prevents a nullpointerexception that occurs when the selected item setIn the list
                //disappears during a search.
                if(blocklist.getSelectionModel().getSelectedItem() == null) {
                    try {
                        blocklist.getSelectionModel().selectFirst();
                        newValue = blocklist.getSelectionModel().getSelectedItem().toString();
                    } catch (NullPointerException e) {
                        newValue = "";
                    }
                }
                blockTitle.setText(newValue);
                if(newValue.equals("AND gate") || newValue.equals("OR gate") || newValue.equals("NAND gate") || newValue.equals("NOR gate")  || newValue.equals("XOR gate")  || newValue.equals("XNOR gate")) {
                    inCount.setDisable(false);
                    inCount.setEditable(true);
                    inCount.setText("0");
                }
                else {
                    inCount.setEditable(false);
                    inCount.setText("");
                    inCount.setDisable(true);
                }
                try {
                    blockDescription.setText(MainExtras.getBlockDescription(newValue));
                } catch (Exception ex) {
                    //this is where we'd try stuff for blocks that aren't basicboxes.
                }
            }
        });
        //make sure the title doesn't mess with the scene proportions
        blockTitle.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(final ObservableValue<? extends String> ov, final String oldValue, final String newValue) {
                blockTitle.setText(MainExtras.shortenTahoma(47,blockTitle.getText()));
            }
        });
        //handle the submit button
        addBlock.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent e) {
                boolean b;
                try {
                    b = allblocks.contains(blocklist.getSelectionModel().getSelectedItem().toString());
                } catch (NullPointerException ec) {
                    b = false;
                }
                if(netlistlist.getValue() == null) {
                    popUpWindow(newStage,"Please select a NetList to add the block.");
                    netlistlist.requestFocus();
                }
                else if(blockName.getText().isEmpty()) {
                    popUpWindow(newStage,"Block cannot be made without a name.");
                    blockName.requestFocus();
                }
                else if(b){
                    String selection = blocklist.getSelectionModel().getSelectedItem().toString();
                    if(nameIsBasicBox(selection)) {
                        //nameIsBasicBox returns true if the selection is contained under the Block class
                        if(nameIsBasicLatch(selection)) {
                            //nameIsBasicLatch returns true if the selection is a latch contained under the Block class
                            try {
                                addBlock(netlistlist.getValue().toString(), new Block(BASIC, blockNameToBasicBox(selection), blockName.getText(),false));
                            } catch (Exception ex) {
                                consoleOutLn("error while making block: name identified as basic latch but basicBox identifier is unknown.");
                            }
                        }
                        else if(selection.equals("Digital HIGH") || selection.equals("Digital LOW")) {
                            //Digital High and Digital Low blocks are a little different in how they're constructed, so they have a special spot here
                            try {
                                addBlock(netlistlist.getValue().toString(),new Block(BASIC, blockNameToBasicBox(selection), blockName.getText(),0));
                            } catch (Exception ex) {
                                consoleOutLn("error while making block: name identified as basic digital constant but basicBox identifier is unknown.");
                            }
                        }
                        else {
                            //all other blocks designed by the Block class are made here (namely, all basic logic gates are made here)
                            try {
                                if(Integer.valueOf(inCount.getText()) < 1) {
                                    popUpWindow(newStage,"Must set at least one input!");
                                }
                                else {
                                    addBlock(netlistlist.getValue().toString(), new Block(BASIC, blockNameToBasicBox(selection), blockName.getText(), Integer.valueOf(inCount.getText())));
                                }
                            } catch (Exception ex) {
                                consoleOutLn("error while making block: name identified as basic gate but basicBox identifier is unknown.");
                            }
                        }
                    }
                    else {
                        //anything that isn't a block inside the Block class is dealt with here.
                        String cons = inCount.getText();
                        if(cons.isEmpty()) {
                            cons = "0";
                        }
                        try {
                            if(Integer.valueOf(cons) < 1 && !inCount.isDisabled()) {
                                popUpWindow(newStage,"Must set at least one input!");
                            }
                            else {
                                addBlock(netlistlist.getValue().toString(), generateNativeBlock(selection, blockName.getText(), Integer.valueOf(cons)));
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            consoleOutLn("Internal Error.");
                        }
                    }
                }
                else {
                    popUpWindow(newStage,"Invalid selection.");
                }
            }



        });
        //don't allow spaces, underscores or colons in the blocknames
        blockName.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(final ObservableValue<? extends String> ov,final String oldValue, final String newValue) {
                blockName.setText(blockName.getText().replaceAll("[ _:]", ""));
            }
        });


        Scene scene = new Scene(grid, 450, 300);
        newStage.setScene(scene);

        //scene.getStylesheets().add
         //       (getClass().getResource("utilsTheme.css").toExternalForm());

        newStage.show();
    }

    @FXML
    public void onAddWire(ActionEvent actionEvent) {
        fullAccordion.setExpandedPane(x2);
        
        Stage newStage = new Stage();
        newStage.initModality(Modality.APPLICATION_MODAL);
        newStage.setTitle("Add Wire");
        newStage.initOwner(mainstage);
        newStage.setResizable(false);

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(10, 10, 10, 10));

        List<String> init = new ArrayList<>();
        List<String> init2 = new ArrayList<>();

        //netlist combo box
        List<String> list = new ArrayList<>();
        for(NetList n : netLists) {
            list.add(n.name());
        }
        ObservableList<String> observableList = FXCollections.observableList(list);
        ComboBox netlistlist = new ComboBox(observableList);
        netlistlist.setPrefWidth(195);
        netlistlist.setPromptText("Select NetList...");
        grid.add(netlistlist,0,2);

        //Text field to search nets
        TextField searchBox = new TextField();
        searchBox.setPromptText("Search...");
        grid.add(searchBox,0,1);

        //List View of all the nets setIn the selected netlist
        ObservableList<String> observableList1 = FXCollections.observableList(init);
        ListView allnetview = new ListView(observableList1);
        grid.add(allnetview,0,0);

        //List View of all the nets added to the wire
        ObservableList<String> observableList2 = FXCollections.observableList(init2);
        ListView chosennetview = new ListView(observableList2);
        grid.add(chosennetview,2,0);

        //Text field for the name of the wire
        TextField wireBox = new TextField();
        wireBox.setPromptText("Wire name");
        grid.add(wireBox,2,1);

        //add and remove buttons
        Button add = new Button();
        add.setText(">");
        add.setMinWidth(30.0);
        Button remove = new Button();
        remove.setText("<");
        remove.setMinWidth(30.0);

        //VBox for the add and remove buttons
        VBox buttonSet = new VBox();
        buttonSet.setSpacing(10.0);
        buttonSet.getChildren().addAll(add,remove);
        buttonSet.setAlignment(Pos.CENTER);
        grid.add(buttonSet,1,0);

        //submit button
        Button submit = new Button();
        submit.setText("Add Wire");
        submit.setMinWidth(90.0);
        grid.add(submit,2,2);

        //search box handler
        try {
            searchBox.textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    List<String> matches = new ArrayList<>();
                    allnetview.getSelectionModel().clearSelection();
                    for(Object p : observableList1) {
                        String j = p.toString();
                        if(j.toLowerCase().replaceAll("[ _-]","").contains(newValue.toLowerCase().replaceAll("[ _-]",""))) {
                            matches.add(j);
                        }
                    }
                    ObservableList<String> observableListn = FXCollections.observableList(matches);
                    allnetview.setItems(observableListn);
                }
            });
        } catch (NullPointerException e) {
            //this catches a nullpointer exception that occurs when the selected item setIn the list moves
            //during a search. the search and everything else about the program still work fine, so
            //it's fine to just leave this empty for now.
        }
        netlistlist.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                //get the netlist
                NetList nl;
                for(NetList n : netLists) {
                    if (n.name().equals(newValue)) {
                        nl = n;

                        List<String> listNets = new ArrayList<>();

                        listNets.addAll(nl.allNets().keySet());

                        allnetview.getItems().clear();
                        allnetview.getItems().addAll(listNets);
                    }
                }
            }
        });
        wireBox.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(final ObservableValue<? extends String> ov,final String oldValue, final String newValue) {
                wireBox.setText(wireBox.getText().replaceAll(" ", ""));
            }
        });
        add.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    if (!allnetview.getSelectionModel().getSelectedItem().toString().equals("null") && !chosennetview.getItems().contains(allnetview.getSelectionModel().getSelectedItem())) {
                        chosennetview.getItems().add(allnetview.getSelectionModel().getSelectedItem());
                    }
                } catch (NullPointerException e) {
                    //this exception doesn't matter
                }
            }
        });
        remove.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                        chosennetview.getItems().remove(chosennetview.getSelectionModel().getSelectedItem());

                } catch (NullPointerException e) {
                    //this exception doesn't matter
                }
            }
        });
        submit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String[] a = new String[chosennetview.getItems().size()];
                for(int i = 0; i < chosennetview.getItems().size(); i++) {
                    a[i] = chosennetview.getItems().get(i).toString();
                }
                addWire(netlistlist.getSelectionModel().getSelectedItem().toString(),wireBox.getText(),a);
                consoleOutLn("new wire " + wireBox.getText() + " setIn netlist " + netlistlist.getSelectionModel().getSelectedItem().toString() + " attaches " + Arrays.asList(a));
                chosennetview.getItems().clear();
                wireBox.clear();
            }
        });

        ColumnConstraints column1 = new ColumnConstraints();
        column1.setHalignment(HPos.LEFT);
        grid.getColumnConstraints().add(column1);
        ColumnConstraints column2 = new ColumnConstraints();
        ColumnConstraints column3 = new ColumnConstraints();
        column3.setHalignment(HPos.RIGHT);
        grid.getColumnConstraints().add(column2);
        grid.getColumnConstraints().add(column3);

        //used to automatically select the netlist that we assume the user is going to be adding wires to.
        switch(thisPass) {
            case CONSOLE:
                netlistlist.getSelectionModel().select(consoleActiveNetList);
                break;
            default:
                netlistlist.getSelectionModel().select(selectedNetList);
                break;
        }

        Scene scene = new Scene(grid, 470, 300);
        newStage.setScene(scene);

        //scene.getStylesheets().add
        //       (getClass().getResource("utilsTheme.css").toExternalForm());

        newStage.show();
    }

    public void onBlockTreeRemoveBlock(ActionEvent actionEvent) {
        List<String> netsToRemove = new ArrayList<>();
        //removing the block from the netlist object
        for(NetList nl : netLists) {
            if(nl.name().equals(selectedNetList)) {
                netsToRemove.addAll(nl.blockNets(selectedBlock).keySet());
                nl.removeBlock(selectedBlock);
            }
        }

        //remove the block's nets frm all wires setIn the treeview
        removeNets(selectedNetList,netsToRemove);

        //removing the block from the treeviews
        for(TreeItem n : blockTreeRoot.getChildren()) {
            if(n.getValue().equals(selectedNetList)) {
                //cycle through the blocks, remove the right one
                for(int i = 0; i < n.getChildren().size(); i++) {
                    if(n.getChildren().get(i).toString().contains("value: " + selectedBlock)) {
                        //now, remove the block from the treeview
                        n.getChildren().remove(n.getChildren().get(i));
                    }
                }
            }
        }
    }

    public void onBlockTreeAddBlock(ActionEvent actionEvent) {
        thisPass = GUI;
        onAddBlock(new ActionEvent(){

        });
        thisPass = NULL;
    }

    public void onBlockTreeRemoveNetList(ActionEvent actionEvent) {
    }

    public void onBlockTreeAddWire(ActionEvent actionEvent) {
    }

    public void onWireTreeRemoveWire(ActionEvent actionEvent) {
        removeWire(selectedNetList,selectedWire);
    }

    public void onWireTreeRemoveWireNet(ActionEvent actionEvent) {
        removeWireNet(selectedNetList,selectedWire,selectedNet);

    }


    //SIMULATOR METHODS
    @FXML
    public void onSeqSimClick(ActionEvent actionEvent) {
        dothings();
    }


    //UTILITIES METHODS
    @FXML
    public void onBooleanSimpClick(ActionEvent actionEvent) {
        Stage newStage = new Stage();
        //window properties
        newStage.initModality(Modality.APPLICATION_MODAL);
        newStage.setTitle("Boolean Simplification");
        newStage.initOwner(mainstage);
        newStage.setResizable(false);

        //Grid setup
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(10, 10, 10, 10));

        //Text items
//        Text scenetitle = new Text("Logic Simplifier");
//        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 14));
//        grid.add(scenetitle, 0, 0, 2, 2);

        Text scenedescription = new Text("Enter a boolean function. Terms must use only one letter.\n"+
                "Complements use apostrophes, just like the normal written form.\n"+
                "Example input: (a'b+bc)'+(b^c)");
        grid.add(scenedescription, 0, 2, 2, 1);

        Text err = new Text();
        err.setFill(Color.RED);
        grid.add(err, 0, 8, 2, 1);


        Label function = new Label("Function:");
        grid.add(function, 0, 3);

        Label simp = new Label("Simplified form:");
        grid.add(simp, 0, 7);

        //Text fields
        TextField userTextField = new TextField();
        grid.add(userTextField, 1, 3);

        final TextField actiontarget = new TextField();
        grid.add(actiontarget, 1, 7);
        actiontarget.setEditable(false);

        //Buttons
        Button btn = new Button("calculate");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().add(btn);
        grid.add(hbBtn, 1, 5);

        userTextField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER)  {
                    enableBusy();
                    String text = userTextField.getText();

                    err.setText("");
                    boolean errorsFound = false;
                    String out = "invalid";
                    if(!userTextField.getText().isEmpty()) {
                        String s = userTextField.getText().replaceAll("\\s+","");

                        Set<Character> hash = new HashSet<>();
                        char ch;
                        for (int i = 0; i < s.length(); i++) {
                            ch = s.charAt(i);
                            if (Character.isLetter(ch)) {
                                hash.add(ch);
                            }
                        }
                        char[] ret = new char[hash.size()];
                        int k = 0;
                        for(char y : hash) ret[k++] = y;
                        consoleOutLn("Logic Interpreter for input phrase ");
                        char[] outChar = new char[1];
                        outChar[0] = 'z';
                        String[] stateString = new String[1];
                        stateString[0] = s;
                        logicInterpreter log = new logicInterpreter(true, ret, outChar, stateString);
                        for(char inNames : hash) consoleOut(String.valueOf(inNames));
                        consoleOutLn(" readOut");
                        consoleOut(log.truthTable('z'));
                        List<String> minterms = log.getMinterms();

                        QuineMcCluskey l = new QuineMcCluskey(minterms, ret,true);
                        l.simplify();
                        out = l.convertToExpression();
                        consoleOutLn(s + " = " + out + "\n");
                        if(!log.getErrors().isEmpty()) {
                            consoleOut(log.getErrors());
                            err.setText("Errors were found. Check the console setIn the main window.");
                            errorsFound = true;
                        }
                        if(!l.getErrors().isEmpty()) {
                            consoleOut(l.getErrors());
                            err.setText("Errors were found. Check the console setIn the main window.");
                            errorsFound = true;
                        }

                    }
                    if(errorsFound) actiontarget.setText("");
                    else actiontarget.setText(out);
                    disableBusy();
                }
                consoleArea.setScrollTop(Double.MAX_VALUE);
            }
        });

        btn.setOnAction(e -> {
            err.setText("");
            boolean errorsFound = false;
            String out = "invalid";
            if(!userTextField.getText().isEmpty()) {
                enableBusy();
                String s = userTextField.getText().replaceAll("\\s+","");

                Set<Character> hash = new HashSet<>();
                char ch;
                for (int i = 0; i < s.length(); i++) {
                    ch = s.charAt(i);
                    if (Character.isLetter(ch)) {
                        hash.add(ch);
                    }
                }
                char[] ret = new char[hash.size()];
                int k = 0;
                for(char y : hash) ret[k++] = y;
                consoleOutLn("Logic Interpreter for input phrase " + s);
                char[] outChar = new char[1];
                outChar[0] = 'z';
                String[] stateString = new String[1];
                stateString[0] = s;
                logicInterpreter log = new logicInterpreter(true, ret, outChar, stateString);
                for(char inNames : hash) consoleOut(String.valueOf(inNames));
                consoleOutLn(" readOut");
                consoleOut(log.truthTable('z'));
                List<String> minterms = log.getMinterms();

                QuineMcCluskey l = new QuineMcCluskey(minterms, ret,true);
                l.simplify();
                out = l.convertToExpression();
                consoleOutLn(s + " = " + out + "\n");
                if(!log.getErrors().isEmpty()) {
                    consoleOut(log.getErrors());
                    err.setText("Errors were found. Check the console setIn the main window.");
                    errorsFound = true;
                }
                if(!l.getErrors().isEmpty()) {
                    consoleOut(l.getErrors());
                    err.setText("Errors were found. Check the console setIn the main window.");
                    errorsFound = true;
                }
            }
            if(errorsFound) actiontarget.setText("");
            else actiontarget.setText(out);
            disableBusy();
            consoleArea.setScrollTop(Double.MAX_VALUE);
        });

        Scene scene = new Scene(grid, 400, 300);
        newStage.setScene(scene);

        //scene.getStylesheets().add
        //        (getClass().getResource("utilsTheme.css").toExternalForm());

        newStage.show();
    }


    //CONSOLE METHODS
    @FXML
    public void onConsoleFieldKeyPressed(KeyEvent keyEvent) {
        //TODO: finish the console commands.
        if (keyEvent.getCode() == KeyCode.ENTER)  {
            if(!consoleField.getText().isEmpty()) {
                enableBusy();
                //consoleOutLn(consoleField.getText());
                if(!consoleHistory.get(histToGet + 1).equals(consoleField.getText())) {
                    consoleHistory.add(consoleField.getText());
                }
                diary.add(consoleField.getText() + "\n");
                console.in(consoleField.getText());

                consoleField.clear();

                switch(console.consoleOut()) {
                    case NEW_NETLIST:
                        consoleOutLn("new netlist with name " + console.conditionAt(2));
                        addNetList(new NetList(console.conditionAt(2)));
                        break;
                    case NEW_BBX:
                        break;
                    case NEW_COMBINATIONAL_SIM:
                        consoleOutLn("new combinational simulation " + console.conditionAt(4) + " for netlist " + console.conditionAt(0));
                        for(NetList n : netLists) {
                            if(n.name().equals(console.conditionAt(0))) addCSimulation(console.conditionAt(0), new CombinationalSimulation(n,console.conditionAt(4)));
                        }
                        break;
                    case NEW_SEQUENTIAL_SIM:
                        consoleOutLn("Sequential simulations have not yet been implemented.");
                        break;
                    case DIARY_ON:
                        consoleOutLn("new diary with filename " + console.conditionAt(2));
                        diary.make(console.conditionAt(2));
                        break;
                    case DIARY_OFF:
                        diary.close();
                        consoleOutLn("diary " + diary.getName() + " closed");
                        break;
                    case ADD_WIRE:
                        try {
                            addWire(console.conditionAt(0), console.conditionAt(3), console.conditionsFrom(4));

                        } catch(ArrayIndexOutOfBoundsException a) {
                            consoleOutLn("syntax for the previous command was incorrect. defaulting to add wire dialog window.");
                            consoleActiveNetList = console.conditionAt(0);
                            thisPass = PassIn.CONSOLE;
                            onAddWire(new ActionEvent() {

                            });
                            thisPass = PassIn.NULL;
                        }

                        break;
                    case ADD_BLOCK:
                        //System.readOut.println("test");
                        consoleActiveNetList = console.conditionAt(0);
                        thisPass = PassIn.CONSOLE;
                        onAddBlock(new ActionEvent() {

                        });
                        thisPass = PassIn.NULL;
                        break;
                    case REM_WIRE:
                        try {
                            removeWireNet(console.conditionAt(0),console.conditionAt(3),console.conditionsFrom(4));
                        } catch (Exception e) {
                            removeWire(console.conditionAt(0),console.conditionAt(3));
                        }
                        break;
                    case SIM_ADD_IN:
                        addCSimNet(console.conditionAt(0),SIMULATOR_IN,console.conditionAt(3));
                        break;
                    case SIM_ADD_OUT:
                        addCSimNet(console.conditionAt(0),SIMULATOR_OUT,console.conditionAt(3));
                        break;
                    case SIM_REM:
                        break;
                    case REM_SIMNET:
                        removeCSimNet(console.conditionAt(0),console.conditionAt(3));
                        break;
                    case SIM_SET:
                        break;
                    case SIM_RUN:
                        break;
                    case SIM_TRUTHTABLE:
                        generateTruthTable(console.conditionAt(0));
                        break;
                    case SIM_FUNCTION:
                        break;
                    case ERROR:
                        consoleOutLn("Syntax Error");
                        break;
                    case HELP:
                        consoleOutLn(FXConsole.helpMessage);
                        break;
                }
                //consoleArea.appendText(consoleField.getText() + "\n");

                disableBusy();
                histToGet = -1;
            }
        }
        else if (keyEvent.getCode() == KeyCode.UP) {
            //System.readOut.print(histToGet + " ");
            if(histToGet < 19 && !consoleHistory.get(histToGet + 1).isEmpty()) histToGet++;
            consoleField.setText(consoleHistory.get(histToGet));
            //System.readOut.println(histToGet);
            Platform.runLater( new Runnable() {
                @Override
                public void run() {
                    consoleField.positionCaret( consoleField.getText().length() );
                }
            });
        }
        else if (keyEvent.getCode() == KeyCode.DOWN) {
            //System.readOut.print(histToGet + " ");
            if(histToGet >= 0) histToGet--;
            if(histToGet == -1) consoleField.clear();
            consoleField.setText(consoleHistory.get(histToGet));
        }
    }

    @FXML
    public void consoleOutLn(String text) {
        if(diary.isEnabled()) {
            if(diary.endsWithNewline()) {
                diary.add("   " + text + "\n");
            }
            else {
                diary.add(text + "\n");
            }
        }
        consoleArea.appendText(text+"\n");
        consoleArea.setScrollTop(Double.MAX_VALUE);
    }

    @FXML
    public void consoleOut(String text) {
        if(diary.isEnabled()) {
            if(diary.endsWithNewline()) {
                diary.add("   " + text);
            }
            else {
                diary.add(text);
            }
        }
        consoleArea.appendText(text);
        consoleArea.setScrollTop(Double.MAX_VALUE);
    }


    //MISCELLANEOUS METHODS
    @FXML
    public void onCloseClick(ActionEvent actionEvent) {
        Platform.exit();
    }

    public void onBlockTreeProperties(ActionEvent actionEvent) {
        Stage newStage = new Stage();
        newStage.initModality(Modality.APPLICATION_MODAL);
        newStage.setTitle("Selection Properties");
        newStage.initOwner(mainstage);
        newStage.setResizable(false);
        newStage.setWidth(300);

        int state = 0;

        /*
        state table:
        0: a netlist is selected.
        1: a single net is selected, without a parent wire or block. (this shouldn't be possible)
        2: a block is selected.
        3: a net is selected within a block parent.
        4: a wire is selected.
        5: a net is selected within a wire parent.
        6: a block is selected within a wire parent, or the other way around.
        7: a net inside of a block and wire parents is selected.
        8: a simulation is selected.
        9-15: repeat 1-7 but tack on "with a simulation parent."
         */

        if(selectedNet != null) state += 1;
        if(selectedBlock != null) state += 2;
        if(selectedWire != null) state += 4;
        if(selectedSimulation != null) state += 8;

        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10,10,10,10));
        grid.setHgap(5);
        grid.setVgap(5);
        grid.setAlignment(Pos.CENTER);

        Label titleLabel = new Label("NetList: ");
        TextField title = new TextField();
        title.setText(selectedNetList);
        title.setEditable(false);
        grid.add(titleLabel,0,0);
        grid.add(title,1,0);

        Label itemLabel = new Label();
        TextField item = new TextField();
        item.setEditable(false);
        switch(state) {
            case 1:
            case 3:
            case 5:
            case 7:
                itemLabel.setText("Net: ");
                item.setText(selectedNet);
                for(NetList n : netLists) {
                    if(n.name().equals(selectedNetList)) {
                        item.appendText(": " + n.allNets().get(selectedNet));
                    }
                }
                grid.add(itemLabel,0,1);
                grid.add(item,1,1);
                break;
            case 2:
            case 6:
                itemLabel.setText("Block: ");
                item.setText(selectedBlock);
                grid.add(itemLabel,0,1);
                grid.add(item,1,1);
                break;
            case 4:
            case 12:
                itemLabel.setText("Wire: ");
                item.setText(selectedWire);
                grid.add(itemLabel,0,1);
                grid.add(item,1,1);
                break;
        }
        Label listedPropertyLabel = new Label();
        TextArea listedProperty = new TextArea();
        listedProperty.setEditable(false);
        listedProperty.setPrefHeight(90);
        switch(state) {
            case 4:
            case 5:
                //show nets attached to the wire
                listedPropertyLabel.setText("Nets: ");
                for(NetList n : netLists) {
                    if(n.name().equals(selectedNetList)) {
                        for(String w : n.allWireProperties().get(selectedWire)) {
                            listedProperty.appendText(w + ": " + n.allNets().get(w) + "\n");
                        }
                    }
                }
                grid.add(listedPropertyLabel,0,2);
                grid.add(listedProperty,1,2);
                break;
            case 2:
                //show wires attached to block
                listedPropertyLabel.setText("Attachments: ");
                for(NetList n : netLists) {
                    if(n.name().equals(selectedNetList)) {
                        for(String net : n.blockNets(selectedBlock).keySet()) {
                            for(String wire : n.netWires(net)) {
                                listedProperty.appendText(net + ": " + wire + ", " + n.allNets().get(net) + "\n");
                            }
                        }
                    }
                }
                grid.add(listedPropertyLabel,0,2);
                grid.add(listedProperty,1,2);
                break;
            case 3:
                //show wires attached to net
                listedPropertyLabel.setText("Attachments: ");
                for(NetList n : netLists) {
                    if(n.name().equals(selectedNetList)) {
                        String p = "";
                        for(String wire : n.netWires(selectedNet)) {
                            p = p.concat(wire + ", ");
                        }
                        if(p.length() > 2) {
                            listedProperty.appendText(p.substring(0, p.length() - 2));
                        }
                    }
                }
                listedProperty.setWrapText(true);
                grid.add(listedPropertyLabel,0,2);
                grid.add(listedProperty,1,2);
                break;
        }
        Label subProperties1Label = new Label("Type: ");
        TextField subProperties1 = new TextField();
        subProperties1.setEditable(false);
        switch(state) {
            case 2:
                for(NetList n : netLists) {
                    if(n.name().equals(selectedNetList)) {
                        String type = n.allBlocks().get(selectedBlock).getBlockType().toString();
                        String subType =  null;
                        if(n.allBlocks().get(selectedBlock) instanceof Block) {
                            subType = ((Block)n.allBlocks().get(selectedBlock)).getBasicBoxesType().toString();
                        }
                        subProperties1.appendText(type);
                        if(subType != null) {
                            subProperties1.appendText(", " + subType);
                        }
                    }
                }
                grid.add(subProperties1Label,0,3);
                grid.add(subProperties1,1,3);
                break;
            case 12:
                for(CombinationalSimulation n : cSims) {
                    if(n.name().equals(selectedSimulation)) {
                        String type = n.getWireType(selectedWire);
                        String value = String.valueOf(n.readSimulationOutput(selectedWire));
                        subProperties1.appendText(type);
                        subProperties1.appendText(", " + value);
                    }
                }
                grid.add(subProperties1Label,0,3);
                grid.add(subProperties1,1,3);
                break;
        }

        ColumnConstraints column1 = new ColumnConstraints(75);
        grid.getColumnConstraints().add(column1);
        Scene scene = new Scene(grid);
        newStage.setScene(scene);

        newStage.show();
    }

    public void onClickDarkTheme(ActionEvent actionEvent) {
        //System.readOut.println(darkThemeRB.isSelected());
        if(darkThemeRB.isSelected()) {
            sf.editConfig(SettingsFile.Settings.THEME, SettingsFile.Configs.DARK);
            //System.readOut.println(SettingsFile.Configs.DARK);
        }
        else {
            sf.editConfig(SettingsFile.Settings.THEME, SettingsFile.Configs.LIGHT);
            //System.readOut.println(SettingsFile.Configs.LIGHT);
        }
        popUpWindow(mainstage,"Theme change will be reflected on next startup.","Information");
    }


    //BACKEND METHODS
    @FXML
    public void addNetList(NetList nl) {
        boolean notAdded = true;
        for(NetList net : netLists) {
            if(net.name().equals(nl.name())) notAdded = false;
        }
        if(notAdded) {
            blockTreeRoot.getChildren().add(new TreeItem<>(nl.name()));
            wireTreeRoot.getChildren().add(new TreeItem<>(nl.name()));
            simTreeRoot.getChildren().add(new TreeItem<>(nl.name()));
            netLists.add(nl);
        }
        else {
            consoleOutLn("netlist of name " + nl.name() + " already exists.");
        }
        console.addToNetLists(nl.name());
    }

    @FXML
    public void addBlock(String nl, NativeBlock bl) {
        boolean notAdded = true;
        for(NetList net : netLists) {
            if (net.name().equals(nl)) {
                for (String b : net.allBlocks().keySet()) {
                    if (bl.blockName().equals(b)) notAdded = false;
                }
                if (notAdded) {
                    net.addBlock(bl);
                    //add the nets to the TreeItem for the block
                    TreeItem<String> p = new TreeItem<>(bl.blockName());
                    for (String blocknet : bl.getNetNames()) {
                        TreeItem<String> a = new TreeItem<>(blocknet);
                        p.getChildren().add(a);
                    }
                    //add the TreeItem containing the block and its nets to the proper netlist Tree
                    for (TreeItem n : blockTreeRoot.getChildren()) {
                        if (n.getValue().equals(nl)) {
                            n.getChildren().add(p);
                        }
                    }
                    consoleOutLn("new block " + bl.blockName() + " added to netlist " + nl);
                }
                else {
                    consoleOutLn("ERROR: a block with name " + bl.blockName() + " already exists!");
                }
            }
        }
    }

    @FXML
    public void addWire(String nl, String wireName, String... conns) {
        for(NetList searchNetLists : netLists) {
            if(searchNetLists.name().equals(nl)) {
                //adding the wire to the desired netlist
                if(!searchNetLists.allWireProperties().keySet().contains(wireName)) {
                    searchNetLists.addWire(wireName,conns);

                    //now, add the wire to the "wires" menu
                    TreeItem<String> wireTitle = new TreeItem<>(wireName);
                    for(String n : conns) {
                        wireTitle.getChildren().add(new TreeItem<String>(n));
                    }
                    for(TreeItem n : wireTreeRoot.getChildren()) {
                        if(n.getValue().equals(nl)) {
                            n.getChildren().add(wireTitle);
                        }
                    }
                    consoleOutLn("new wire " + wireName + " in netlist " + nl + " attaches " + Arrays.asList(conns));
                }
                else {
                    consoleOutLn("Wire with name " + wireName + " already exists setIn this netlist!");
                }
            }
        }
    }

    @FXML
    public void removeWire(String nl, String wireName) {
        for(NetList searchNetLists : netLists) {
            if(searchNetLists.name().equals(nl)) {
                //remove the wire to the desired netlist
                searchNetLists.removeWire(wireName);
            }
        }

        //removing the wire from the wire tree
        for(TreeItem n : wireTreeRoot.getChildren()) {
            if(n.getValue().equals(nl)) {
                for(int i = 0; i < n.getChildren().size(); i++) {
                    if(n.getChildren().get(i).toString().contains("value: " + wireName)) {
                        n.getChildren().remove(n.getChildren().get(i));
                    }
                }
            }
        }

    }

    @FXML
    public void removeNets(String nl, String... netNames) {
        //removing the net from all wireTrees
        for (String netName : netNames) {
            for (TreeItem n : wireTreeRoot.getChildren()) {
                if (n.getValue().equals(nl)) {
                    for (int i = 0; i < n.getChildren().size(); i++) {
                        //System.readOut.println(n.getChildren().get(i));
                        if (n.getChildren().get(i) instanceof TreeItem) {
                            //System.readOut.println("setIn the instance");
                            TreeItem p = (TreeItem) n.getChildren().get(i);
                            for (int j = 0; j < p.getChildren().size(); j++) {
                                //System.readOut.println(p.getChildren().get(j));
                                if (p.getChildren().get(j).toString().contains(netName)) {
                                    p.getChildren().remove(p.getChildren().get(j));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @FXML
    public void removeNets(String nl, List<String> netNames) {
        //removing the net from all wireTrees
        for (String netName : netNames) {
            for (TreeItem n : wireTreeRoot.getChildren()) {
                if (n.getValue().equals(nl)) {
                    for (int i = 0; i < n.getChildren().size(); i++) {
                        //System.readOut.println(n.getChildren().get(i));
                        if (n.getChildren().get(i) instanceof TreeItem) {
                            //System.readOut.println("setIn the instance");
                            TreeItem p = (TreeItem) n.getChildren().get(i);
                            for (int j = 0; j < p.getChildren().size(); j++) {
                                //System.readOut.println(p.getChildren().get(j));
                                if (p.getChildren().get(j).toString().contains(netName)) {
                                    p.getChildren().remove(p.getChildren().get(j));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @FXML
    public void removeWireNet(String nl, String wireName, String... netName) {
        for(String nn : netName) {
            for (NetList n : netLists) {
                if (n.name().equals(nl)) {
                    n.removeWireConnection(wireName, nn);
                }
            }

            //removing the wirenet from its wire setIn the tree
            for (TreeItem n : wireTreeRoot.getChildren()) {
                if (n.getValue().equals(nl)) {
                    for (int i = 0; i < n.getChildren().size(); i++) {
                        if (n.getChildren().get(i).toString().contains("value: " + wireName)) {
                            //n.getChildren().get(i) is the TreeItem of the wire
                            if (n.getChildren().get(i) instanceof TreeItem) {
                                TreeItem p = (TreeItem) n.getChildren().get(i);
                                for (int j = 0; j < p.getChildren().size(); j++) {
                                    if (p.getChildren().get(j).toString().contains(nn)) {
                                        p.getChildren().remove(p.getChildren().get(j));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @FXML
    public void addCSimulation(String nl, CombinationalSimulation csim) {
        console.addToSims(csim.name());
        boolean doAdd = true;
        for(CombinationalSimulation c : cSims) {
            if(c.name().equals(csim.name())) {
                doAdd = false;
            }
        }
        if(doAdd) {
            for (NetList searchNetLists : netLists) {
                if (searchNetLists.name().equals(nl)) {
                    cSims.add(csim);

                    TreeItem<String> simName = new TreeItem<>(csim.name());
                    for (String n : csim.wireNames()) {
                        simName.getChildren().add(new TreeItem<String>(n));
                    }
                    for (TreeItem n : simTreeRoot.getChildren()) {
                        if (n.getValue().equals(nl)) {
                            n.getChildren().add(simName);
                        }
                    }
                }
            }
        }
        else {
            consoleOutLn("A simulation with this name aready exists. Simulation cannot be created.");
        }
    }

    @FXML
    public void addCSimNet(String sim, NetList.netType n, String wireName) {
        for(CombinationalSimulation c : cSims) {
            if(c.name().equals(sim)) {
                consoleOutLn(c.addSimulationNet(n,wireName));
                //System.readOut.println("added " + n + " named " + wireName + " to " + c.name());
            }
        }

        for(TreeItem root : simTreeRoot.getChildren()) {
            for(Object sims : root.getChildren()) {
                if(sims instanceof TreeItem) {
                    if(((TreeItem) sims).getValue().equals(sim)) {
                        TreeItem<String> newWire = new TreeItem<>(wireName);
                        ((TreeItem)sims).getChildren().add(newWire);
                    }
                }
            }
        }
    }

    @FXML
    public void removeCSimNet(String sim, String wireName) {
        for(CombinationalSimulation c : cSims) {
            if(c.name().equals(sim)) {
                c.removeSimulationNet(wireName);
            }
        }
    }

    @FXML
    public void generateTruthTable(String sim) {
        for(CombinationalSimulation c : cSims) {
            if(c.name().equals(sim)) {
                consoleOutLn(c.truthTable());
            }
        }
    }

    void enableBusy() {
        progressBar.setProgress(-1);
    }

    void disableBusy() {
        progressBar.setProgress(0);
    }
}