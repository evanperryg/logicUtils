# logicUtils
simplification of natural-language boolean algebra, simulation of digital circuits in a visual environment

logicUtils is a suite of tools for Electrical Engineers, including digital logic simulation and boolean simplification. This application can be thought of as a version of MATLAB for Electrical Engineers. It is a console-centeric suite for calculating the properties of user-entered digital circuits.

Using JavaFX, everything is wrapped up in an aesthetically pleasing package.

Features:
   -Create netlists (a virtual circuit)
   -Add blocks to netlists, including standard logic gates, latches, flip flops, and MSI components
   -Define simulations for netlists and get truth tables and simplified boolean functions for circuit outputs
   -Simplify boolean functions entered in natural style
   -MATLAB-esque object manipulation through a console

Work-In-Progress features:
   -Click-and-drag block diagram creation with graphical simulations of digital circuits
   -Import and export Verilog files that implement the structural model
